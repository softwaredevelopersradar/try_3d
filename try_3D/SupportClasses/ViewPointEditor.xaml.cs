﻿using System;
using System.Windows;
using System.Windows.Controls;

using Arction.Wpf.SemibindableCharting;
using Arction.Wpf.SemibindableCharting.Views.View3D;
using Arction.Wpf.SemibindableCharting.Views.ViewPie3D;

namespace try_3D
{
	/// <summary>
	/// Interaction logic for UserControlViewPointEditor.xaml.
	/// </summary>
	public partial class ViewPointEditor : UserControl
	{
		// Related chart.
		private LightningChartUltimate _chart;
		// Related view.
		private View3DBase m_view;

		private RoutedPropertyChangedEventHandler<Double> m_distanceSliderValueChanged;
		private RoutedPropertyChangedEventHandler<Double> m_horizontalSliderValueChanged;
		private RoutedPropertyChangedEventHandler<Double> m_sideSliderValueChanged;
		private RoutedPropertyChangedEventHandler<Double> m_verticalSliderValueChanged;
        
        /// <summary>
        /// Keeps track if the chagne of camera values originate from this side and 
        /// causes skipping of the UI update. this way we do not override the values 
        /// we're to send in to the camera.
        /// </summary>
        private bool m_bOwnChange = false;

		public ViewPointEditor()
		{
			InitializeComponent();

			_chart = null;
			m_view = null;

			m_distanceSliderValueChanged = new RoutedPropertyChangedEventHandler<Double>(sliderDistance_ValueChanged);
			m_horizontalSliderValueChanged = new RoutedPropertyChangedEventHandler<Double>(sliderHorizontalRotation_ValueChanged);
			m_sideSliderValueChanged = new RoutedPropertyChangedEventHandler<Double>(sliderSideRotation_ValueChanged);
			m_verticalSliderValueChanged = new RoutedPropertyChangedEventHandler<Double>(sliderVerticalRotation_ValueChanged);

			EnabledChangedEvents();
		}

		/// <summary>
		/// Chart.
		/// </summary>
		public LightningChartUltimate Chart
		{
			get
			{
				return _chart;
			}

			set
			{
				_chart = value;

				if (_chart != null)
				{
					switch (_chart.ActiveView)
					{
						case ActiveView.View3D:
							m_view = _chart.View3D;
							if (m_view != null)
								((View3D)m_view).CameraViewChanged += new View3D.CameraViewChangedHandler(ViewPointEditor_CameraViewChanged);
							break;
						case ActiveView.ViewPie3D:
							m_view = _chart.ViewPie3D;
							if (m_view != null)
								((ViewPie3D)m_view).CameraViewChanged += new ViewPie3D.CameraViewChangedHandler(ViewPointEditor_CameraViewChanged);
							break;
						default:
							m_view = null;
							break;
					}
				}
				
				SetControlValuesFromChart();
				SetViewPoint();
			}
		}

		private void DisableChangedEvents()
		{
			checkBoxOrthogonal.Checked -= checkBoxOrthogonal_CheckedChanged;
			checkBoxOrthogonal.Unchecked -= checkBoxOrthogonal_CheckedChanged;
			sliderDistance.ValueChanged -= m_distanceSliderValueChanged;
			sliderHorizontalRotation.ValueChanged -= m_horizontalSliderValueChanged;
			sliderSideRotation.ValueChanged -= m_sideSliderValueChanged;
			sliderVerticalRotation.ValueChanged -= m_verticalSliderValueChanged;
			
		}

		private void EnabledChangedEvents()
		{
			checkBoxOrthogonal.Checked += checkBoxOrthogonal_CheckedChanged;
			checkBoxOrthogonal.Unchecked += checkBoxOrthogonal_CheckedChanged;
			sliderDistance.ValueChanged += m_distanceSliderValueChanged;
			sliderHorizontalRotation.ValueChanged += m_horizontalSliderValueChanged;
			sliderSideRotation.ValueChanged += m_sideSliderValueChanged;
			sliderVerticalRotation.ValueChanged += m_verticalSliderValueChanged;
		
		}

		/// <summary>
		/// Set control values from chart properties.
		/// </summary>
		private void SetControlValuesFromChart()
		{
			if (m_view != null && !m_bOwnChange)
			{
                DisableChangedEvents();

                checkBoxOrthogonal.IsChecked = m_view.Camera.OrthographicCamera;


                double dRotation = m_view.Camera.RotationX;
                double dChange = 180 * (dRotation < 0 ? -1 : 1);
                dRotation = (Int32)((dRotation + dChange) % 360D - dChange);
                if (dRotation > sliderVerticalRotation.Maximum)
                {
                    dRotation = sliderVerticalRotation.Maximum;
                }
                if (dRotation < sliderVerticalRotation.Minimum)
                {
                    dRotation = sliderVerticalRotation.Minimum;
                }
                sliderVerticalRotation.Value = (Int32)Math.Round(dRotation);

                dRotation = m_view.Camera.RotationY;
                dChange = 180 * (dRotation < 0 ? -1 : 1);
                sliderHorizontalRotation.Value = (Int32)((dRotation + dChange) % 360D - dChange);

                dRotation = m_view.Camera.RotationZ;
                dChange = 180 * (dRotation < 0 ? -1 : 1);
                sliderSideRotation.Value = (Int32)((dRotation + dChange) % 360D - dChange);

                sliderDistance.Value = (Int32)m_view.Camera.ViewDistance;

                EnabledChangedEvents();
			}
		}

		/// <summary>
		/// Set all the view point parameters
		/// </summary>
		private void SetViewPoint()
		{
			if (_chart == null || m_view == null)
			{
				return;
			}
			
            m_bOwnChange = true;
			
			// Disable rendering, strongly recommended before updating chart properties.
			_chart.BeginUpdate();

			m_view.Camera.OrthographicCamera = checkBoxOrthogonal.IsChecked.Value;

            m_view.Camera.SetEulerAngles(sliderVerticalRotation.Value,
                                         sliderHorizontalRotation.Value,
                                         sliderSideRotation.Value);

			m_view.Camera.ViewDistance = sliderDistance.Value;
			
			//Allow chart rendering
			_chart.EndUpdate();

            m_bOwnChange = false;
            SetControlValuesFromChart();
		}

		/// <summary>
		/// Set view distance.
		/// </summary>
		/// <param name="distance">distance</param>
		public void SetViewDistance(int distance)
		{
			sliderDistance.Value = distance;

			SetViewPoint();
		}

		/// <summary>
		/// Camera view changed, update control values.
		/// </summary>
		/// <param name="newCameraViewPoint">new camera</param>
		/// <param name="view">related 3D view</param>
		/// <param name="chart">related chart</param>
		private void ViewPointEditor_CameraViewChanged(Camera3D newCameraViewPoint, View3D view, LightningChartUltimate chart)
		{
			SetControlValuesFromChart();
		}

		/// <summary>
		/// Camera view changed, update control values.
		/// </summary>
		/// <param name="newCameraViewPoint">new camera</param>
		/// <param name="view">related 3D pie view</param>
		/// <param name="chart">related chart</param>
		private void ViewPointEditor_CameraViewChanged(Camera3D newCameraViewPoint, ViewPie3D view, LightningChartUltimate chart)
		{
			SetControlValuesFromChart();
		}

		/// <summary>
		/// Show view from top.
		/// </summary>
		/// <param name="sender">sender</param>
		/// <param name="e">arguments</param>
		private void buttonTopView2D_Click(object sender, RoutedEventArgs e)
		{
			DisableChangedEvents();

			checkBoxOrthogonal.IsChecked = true;
            sliderDistance.IsEnabled = false;

            sliderVerticalRotation.Value = 89;
			sliderHorizontalRotation.Value = 0;
			sliderSideRotation.Value = 90;

			SetViewPoint();

			EnabledChangedEvents();
		}

		/// <summary>
		/// Rotate camera on x-axis.
		/// </summary>
		/// <param name="sender">sender</param>
		/// <param name="e">arguments</param>
		private void sliderVerticalRotation_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			m_view.Camera.RotationX = sliderVerticalRotation.Value;
		}

		/// <summary>
		/// Rotate camera on y-axis.
		/// </summary>
		/// <param name="sender">sender</param>
		/// <param name="e">arguments</param>
		private void sliderHorizontalRotation_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			m_view.Camera.RotationY = sliderHorizontalRotation.Value;
		}

		/// <summary>
		/// Rotate camera on z-axis.
		/// </summary>
		/// <param name="sender">sender</param>
		/// <param name="e">arguments</param>
		private void sliderSideRotation_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			m_view.Camera.RotationZ = sliderSideRotation.Value;
		}

		/// <summary>
		/// Update camera distance.
		/// </summary>
		/// <param name="sender">sender</param>
		/// <param name="e">arguments</param>
		private void sliderDistance_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			if (m_view != null)
			{
				m_view.Camera.ViewDistance = sliderDistance.Value;
			}
		}

		/// <summary>
		/// Toggle orthogonal camera.
		/// </summary>
		/// <param name="sender">sender</param>
		/// <param name="e">arguments</param>
		private void checkBoxOrthogonal_CheckedChanged(object sender, RoutedEventArgs e)
		{
			if (m_view != null)
			{
				m_view.Camera.OrthographicCamera = checkBoxOrthogonal.IsChecked.Value;
				sliderDistance.IsEnabled = !(checkBoxOrthogonal.IsChecked.Value);
			}
		}
	}
}
