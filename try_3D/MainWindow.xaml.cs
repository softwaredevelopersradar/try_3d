﻿using Arction.Wpf.SemibindableCharting;
using Arction.Wpf.SemibindableCharting.Annotations;
using Arction.Wpf.SemibindableCharting.Series3D;
using Arction.Wpf.SemibindableCharting.Views.View3D;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace try_3D
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private void InitializeDeploymentKey()
        {
            string deploymentKey = "lgCAABW2ij + vBNQBJABVcGRhdGVhYmxlVGlsbD0yMDE5LTA2LTE1I1JldmlzaW9uPTACgD + BCRGnn7c6dwaDiJovCk5g5nFwvJ + G60VSdCrAJ + jphM8J45NmxWE1ZpK41lW1wuI4Hz3bPIpT7aP9zZdtXrb4379WlHowJblnk8jEGJQcnWUlcFnJSl6osPYvkxfq / B0dVcthh7ezOUzf1uXfOcEJ377 / 4rwUTR0VbNTCK601EN6 / ciGJmHars325FPaj3wXDAUIehxEfwiN7aa7HcXH6RqwOF6WcD8voXTdQEsraNaTYbIqSMErzg6HFsaY5cW4IkG6TJ3iBFzXCVfvPRZDxVYMuM + Q5vztCEz5k + Luaxs + S + OQD3ELg8 + y7a / Dv0OhSQkqMDrR / o7mjauDnZVt5VRwtvDYm6kDNOsNL38Ry / tAsPPY26Ff3PDl1ItpFWZCzNS / xfDEjpmcnJOW7hmZi6X17LM66whLUTiCWjj81lpDi + VhBSMI3a2I7jmiFONUKhtD91yrOyHrCWObCdWq + F5H4gjsoP0ffEKcx658a3ZF8VhtL8d9 + B0YtxFPNBQs =";
            //Set Deployment Key for semi - bindable chart, if you use it
            Arction.Wpf.SemibindableCharting.LightningChartUltimate.SetDeploymentKey(deploymentKey);
        }

        public MainWindow()
        {
            InitializeDeploymentKey();
            InitializeComponent();

            //Init2();
            Init2plus();

            //Init3();

            //LoadModelsFromResources();

            viewPointEditor1.Chart = _chart;

            InitTimer();

            // CreateWaterSurface2();

            //InitRectangles(); //стены на поверхностях


            Init3DDataSeries();


            //InitCoorsTupleList();
            InitCoorsTupleList2();

            LoadFromYaml();
        }

        Settings3D settings3D;

        private void LoadFromYaml()
        {
            settings3D = YamlLoad();
            myIP.Text = settings3D.UDPmyIP;
            myPort.Text = settings3D.UDPmyPort.ToString();
            remoteIP.Text = settings3D.UDPremoteIP;
            remotePort.Text = settings3D.UDPremotePort.ToString();
        }


        private void InitDemo()
        {
            //Load obj
            {
                //Disable rendering, strongly recommended before updating chart properties
                _chart.BeginUpdate();

                MeshModel model = new MeshModel(
                _chart.View3D,
                Axis3DBinding.Primary,
                Axis3DBinding.Primary,
                Axis3DBinding.Primary);

                string temp = System.Reflection.Assembly.GetEntryAssembly().Location;
                var lastindex = temp.LastIndexOf("\\");
                temp = temp.Substring(0, lastindex);
                lastindex = temp.LastIndexOf("\\");
                temp = temp.Substring(0, lastindex);
                lastindex = temp.LastIndexOf("\\");
                temp = temp.Substring(0, lastindex);
                temp = temp + "\\Resources\\3d_metashape_1M.obj";
                model.LoadFromFile(temp);

                model.Position.X = (float)_chart.View3D.XAxisPrimary3D.Minimum + (float)(_chart.View3D.XAxisPrimary3D.Maximum - _chart.View3D.XAxisPrimary3D.Minimum) * 0.5f;
                model.Position.Y = 0;
                model.Position.Z = (float)_chart.View3D.ZAxisPrimary3D.Minimum + (float)(_chart.View3D.ZAxisPrimary3D.Maximum - _chart.View3D.ZAxisPrimary3D.Minimum) * 0.5f;

                model.Rotation.SetValues(0, 0, 0);

                model.Size.SetValues(1f, 1f, 1f);

                model.MouseInteraction = false;
                _chart.View3D.MeshModels.Add(model);

                //Allow chart rendering
                _chart.EndUpdate();
            }
            //size
            MeshSize_Click(this, null);
            //rotation
            Yrotation_Click(this, null);
            //visibility
            Button10_Click(this, null);
            //uav aeroscope
            Button13_Click(this, null);
            //Button21_Click(this, null);
            //uav RDM
            AddUAV(ref DroneModelRDM);
            //udp
            UDP_Click(this, null);
            //clear right panel
            yBox.Clear();
            meshSizeBox.Clear();
            //hide right panel
            Button26_Click(this, null);
        }

        UDPReceiver.UDPReceiver uDPReceiver;

        private void UDP_Click(object sender, RoutedEventArgs e)
        {
            InitUDPReceiver();
        }

        private void InitUDPReceiver()
        {
            //TryParse
            var myIp = IPAddress.Parse(myIP.Text);
            var myport = Int32.Parse(myPort.Text);
            var remoteIp = IPAddress.Parse(remoteIP.Text);
            var remoteport = Int32.Parse(remotePort.Text);
            byte addrSender = 0;
            byte addrRecipient = 0;

            uDPReceiver = new UDPReceiver.UDPReceiver(myIp, myport, remoteIp, remoteport, addrSender, addrRecipient);

            uDPReceiver.OnGetCoordRDM += UDPReceiver_OnGetCoordRDM;
            uDPReceiver.OnGetCoordAero += UDPReceiver_OnGetCoordAero;

            uDPReceiver.OnConnect += UDPReceiver_OnConnect;
            uDPReceiver.OnDisconnect += UDPReceiver_OnDisconnect;

            uDPReceiver.Connect();
        }

        private void UDPReceiver_OnConnect(object sender, bool e)
        {
            UPD.Background = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Green);
        }
        private void UDPReceiver_OnDisconnect(object sender, bool e)
        {
            UPD.Background = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Red);
        }


        private void UDPReceiver_OnGetCoordRDM(object sender, UDPReceiver.Coord e)
        {
            point5 += 2.0;

            if (point5 >= 360) point5 = point5 - 360;

            float x = (float)e.latitude;
            float z = (float)e.longitude;
            float y = (float)e.altitude;

            DispatchIfNecessary(() =>
            {
                if (altCheckBox.IsChecked.Value)
                {
                    var ywidth = (float)(_chart.View3D.YAxisPrimary3D.Maximum - _chart.View3D.YAxisPrimary3D.Minimum);
                    y = (float)Math.Cos(2 * Math.PI * point5 / 360) * (ywidth / 4) + (ywidth / 2);
                }

                DroneModelRDM.Position.X = (float)x;
                DroneModelRDM.Position.Y = (float)y;
                DroneModelRDM.Position.Z = (float)z;

                _chart.View3D.Annotations[1].TargetAxisValues.SetValues(DroneModelRDM.Position.X, DroneModelRDM.Position.Y, DroneModelRDM.Position.Z);
                _chart.View3D.Annotations[1].Text = "X: " + DroneModelRDM.Position.X.ToString("F6") + "\nY: " + DroneModelRDM.Position.Y.ToString("F0") + "\nZ: " + DroneModelRDM.Position.Z.ToString("F6");


                if (lseriesPoint3DsRDM.Count < tail1)
                {
                    lseriesPoint3DsRDM.Add(new SeriesPoint3D(x, y, z));
                }
                else
                {
                    lseriesPoint3DsRDM.RemoveAt(0);
                    lseriesPoint3DsRDM.Add(new SeriesPoint3D(x, y, z));
                }

                if (lseries2Point3DsRDM.Count < tail2)
                {
                    lseries2Point3DsRDM.Add(new SeriesPoint3D(x, y, z));
                    lseries2Point3DsRDM.Add(new SeriesPoint3D(x, 0, z));
                    lseries2Point3DsRDM.Add(new SeriesPoint3D(x, y, z));
                }
                else
                {
                    lseries2Point3DsRDM.RemoveAt(0);
                    lseries2Point3DsRDM.RemoveAt(0);
                    lseries2Point3DsRDM.RemoveAt(0);
                    lseries2Point3DsRDM.Add(new SeriesPoint3D(x, y, z));
                    lseries2Point3DsRDM.Add(new SeriesPoint3D(x, 0, z));
                    lseries2Point3DsRDM.Add(new SeriesPoint3D(x, y, z));
                }

                dataSeriesRDM.Points = lseriesPoint3DsRDM.ToArray();

                dataSeries2RDM.Points = lseries2Point3DsRDM.ToArray();

            });
        }

        private void UDPReceiver_OnGetCoordAero(object sender, UDPReceiver.Coord e)
        {
            point4 += 2.0;

            if (point4 >= 360) point4 = point4 - 360;

            float x = (float)e.latitude;
            float z = (float)e.longitude;
            float y = (float)e.altitude;

            DispatchIfNecessary(() =>
            {
                if (altCheckBox.IsChecked.Value)
                {
                    var ywidth = (float)(_chart.View3D.YAxisPrimary3D.Maximum - _chart.View3D.YAxisPrimary3D.Minimum);
                    y = (float)Math.Cos(2 * Math.PI * point4 / 360) * (ywidth / 4) + (ywidth / 2);
                }

                DroneModel.Position.X = (float)x;
                DroneModel.Position.Y = (float)y;
                DroneModel.Position.Z = (float)z;

                _chart.View3D.Annotations[0].TargetAxisValues.SetValues(DroneModel.Position.X, DroneModel.Position.Y, DroneModel.Position.Z);
                _chart.View3D.Annotations[0].Text = "X: " + DroneModel.Position.X.ToString("F6") + "\nY: " + DroneModel.Position.Y.ToString("F0") + "\nZ: " + DroneModel.Position.Z.ToString("F6");


                if (lseriesPoint3Ds.Count < tail1)
                {
                    lseriesPoint3Ds.Add(new SeriesPoint3D(x, y, z));
                }
                else
                {
                    lseriesPoint3Ds.RemoveAt(0);
                    lseriesPoint3Ds.Add(new SeriesPoint3D(x, y, z));
                }

                if (lseries2Point3Ds.Count < tail2)
                {
                    lseries2Point3Ds.Add(new SeriesPoint3D(x, y, z));
                    lseries2Point3Ds.Add(new SeriesPoint3D(x, 0, z));
                    lseries2Point3Ds.Add(new SeriesPoint3D(x, y, z));
                }
                else
                {
                    lseries2Point3Ds.RemoveAt(0);
                    lseries2Point3Ds.RemoveAt(0);
                    lseries2Point3Ds.RemoveAt(0);
                    lseries2Point3Ds.Add(new SeriesPoint3D(x, y, z));
                    lseries2Point3Ds.Add(new SeriesPoint3D(x, 0, z));
                    lseries2Point3Ds.Add(new SeriesPoint3D(x, y, z));
                }

                dataSeries.Points = lseriesPoint3Ds.ToArray();

                dataSeries2.Points = lseries2Point3Ds.ToArray();

            });
        }

        List<AeroScopeEmulator.Emulator.AeroScope> aeroScopes = new List<AeroScopeEmulator.Emulator.AeroScope>();
        List<AeroScopeEmulator.Emulator.AeroScope> rdmList = new List<AeroScopeEmulator.Emulator.AeroScope>();

        private void LoadAeroScopeEmulator(string name)
        {
            string temp = System.Reflection.Assembly.GetEntryAssembly().Location;
            var lastindex = temp.LastIndexOf("\\");
            temp = temp.Substring(0, lastindex + 1);
            temp = temp + name + ".txt";

            aeroScopes = AeroScopeEmulator.Emulator.GetSamples(temp);
            int tail = (aeroScopes.Count / 100);
            tail1 = tail * 100;
            tail2 = tail1 * 3;
        }

        int tail1 = 100;
        int tail2 = 300;


        private void Init3DDataSeries()
        {
            dataSeries.PointsVisible = false;
            dataSeries.LineVisible = true;
            dataSeries.LineStyle.Width = 0.3f;
            dataSeries.Title.Text = "AeroScope position";
            dataSeries.MouseInteraction = true;
            dataSeries.LineStyle.Color = System.Windows.Media.Color.FromRgb(255, 0, 0);

            //dataSeries2.PointsVisible = false;
            //dataSeries2.LineVisible = true;
            //dataSeries2.LineStyle.Width = 0.3f;
            dataSeries2.Title.Text = "AeroScope trajectory";
            //dataSeries2.MouseInteraction = true;
            dataSeries2.LineStyle.Color = System.Windows.Media.Color.FromRgb(255, 255, 0);

            dataSeriesRDM.PointsVisible = false;
            dataSeriesRDM.LineVisible = true;
            dataSeriesRDM.LineStyle.Width = 0.3f;
            dataSeriesRDM.Title.Text = "RDM position";
            dataSeriesRDM.MouseInteraction = true;
            dataSeriesRDM.LineStyle.Color = System.Windows.Media.Color.FromRgb(0, 0, 255);

            //dataSeries2RDM.PointsVisible = false;
            //dataSeries2RDM.LineVisible = true;
            //dataSeries2RDM.LineStyle.Width = 0.3f;
            dataSeries2RDM.Title.Text = "RDM trajectory";
            //dataSeries2RDM.MouseInteraction = true;
            dataSeries2RDM.LineStyle.Color = System.Windows.Media.Color.FromRgb(0, 255, 0);
        }

        private MapCoordinate m_mapCoordUpperLeft;
        private MapCoordinate m_mapCoordLowerRight;

        private void Init1()
        {
            m_mapCoordUpperLeft = new MapCoordinate(37, 46, 48, LatitudePostfix.N, 125, 18, 0, LongitudePostfix.E);
            m_mapCoordLowerRight = new MapCoordinate(37, 40, 48, LatitudePostfix.N, 125, 21, 36, LongitudePostfix.E);
        }

        private void InitMapCoords()
        {
            m_mapCoordUpperLeft = new MapCoordinate(55.964909, 23.518057);
            m_mapCoordLowerRight = new MapCoordinate(52.936456, 31.560286);
        }


        private MapCoordinate m_mapCoordCorner;
        private MapCoordinate m_mapCoordMaxLat;
        private MapCoordinate m_mapCoordMaxLon;

        private void InitMapCoords3d_l()
        {
            //m_mapCoordCorner = new MapCoordinate(53.928985, 27.635328);
            m_mapCoordCorner = new MapCoordinate(53.928985, 27.632828);
            m_mapCoordMaxLat = new MapCoordinate(53.935037, 27.632366);
            m_mapCoordMaxLon = new MapCoordinate(53.931695, 27.640023);
        }

        private void InitMapCoords1M()
        {
            m_mapCoordCorner = new MapCoordinate(53.930210, 27.632743);
            m_mapCoordMaxLat = new MapCoordinate(53.934570, 27.632773);
            m_mapCoordMaxLon = new MapCoordinate(53.930162, 27.640043);
        }

        private void Init2()
        {
            InitMapCoords();

            _chart.ChartBackground.Color = Colors.DimGray;
            _chart.ChartBackground.GradientColor = Colors.Black;
            _chart.ChartBackground.GradientFill = GradientFill.Radial;

            _chart.View3D.Lights = View3D.CreateDefaultLights();

            _chart.View3D.Lights[1].Location.SetValues(60, 50, 80);


            //X axis shows longitude
            _chart.View3D.XAxisPrimary3D.SetRange(m_mapCoordUpperLeft.Longitude, m_mapCoordLowerRight.Longitude);
            _chart.View3D.XAxisPrimary3D.ValueType = AxisValueType.MapCoordsDegMinSecNESW;
            _chart.View3D.XAxisPrimary3D.Title.Text = "Longitude";
            _chart.View3D.XAxisPrimary3D.LabelsColor = Colors.White;
            _chart.View3D.XAxisPrimary3D.Title.Color = Colors.LightCyan;
            _chart.View3D.XAxisPrimary3D.LabelTicksGap = 20;

            _chart.View3D.XAxisPrimary3D.MouseDragSnapToDiv = false;
            _chart.View3D.XAxisPrimary3D.ScaleNibs = new Arction.Wpf.SemibindableCharting.Axes.AxisDragNib3D();

            _chart.View3D.XAxisPrimary3D.MouseInteraction = false;
            _chart.View3D.XAxisPrimary3D.MouseDragSnapToDiv = false;
            _chart.View3D.XAxisPrimary3D.MouseScaling = false;
            _chart.View3D.XAxisPrimary3D.MouseScrolling = false;

            //Y axis shows the depth
            _chart.View3D.YAxisPrimary3D.SetRange(0, 1000);
            _chart.View3D.YAxisPrimary3D.Title.Text = "Height";
            _chart.View3D.YAxisPrimary3D.LabelsColor = Colors.White;
            _chart.View3D.YAxisPrimary3D.Title.Color = Colors.LightCyan;
            _chart.View3D.YAxisPrimary3D.Units.Text = "m";
            _chart.View3D.YAxisPrimary3D.LabelTicksGap = 20;

            _chart.View3D.YAxisPrimary3D.MouseDragSnapToDiv = false;

            _chart.View3D.YAxisPrimary3D.MouseInteraction = false;
            _chart.View3D.YAxisPrimary3D.MouseDragSnapToDiv = false;
            _chart.View3D.YAxisPrimary3D.MouseScaling = false;
            _chart.View3D.YAxisPrimary3D.MouseScrolling = false;


            //Z axis shows latitude
            _chart.View3D.ZAxisPrimary3D.SetRange(m_mapCoordLowerRight.Latitude, m_mapCoordUpperLeft.Latitude);
            _chart.View3D.ZAxisPrimary3D.ValueType = AxisValueType.MapCoordsDegMinSecNESW;
            _chart.View3D.ZAxisPrimary3D.Title.Text = "Latitude";
            _chart.View3D.ZAxisPrimary3D.LabelsColor = Colors.White;
            _chart.View3D.ZAxisPrimary3D.Title.Color = Colors.LightCyan;
            _chart.View3D.ZAxisPrimary3D.LabelTicksGap = 20;

            _chart.View3D.ZAxisPrimary3D.MouseDragSnapToDiv = false;

            _chart.View3D.ZAxisPrimary3D.MouseInteraction = false;
            _chart.View3D.ZAxisPrimary3D.MouseDragSnapToDiv = false;
            _chart.View3D.ZAxisPrimary3D.MouseScaling = false;
            _chart.View3D.ZAxisPrimary3D.MouseScrolling = false;


            //Hide walls 
            List<WallBase> walls = _chart.View3D.GetWalls();
            foreach (WallBase wall in walls)
            {
                wall.Visible = false;
            }

            Bitmap bitmap = (Bitmap)ChartTools.ImageFromResource("Resources.SeaBottom.jpg", Assembly.GetExecutingAssembly());
            //CreateSeaBottomSurface(bitmap);
            //CreateSeaBottomSurface2(bitmap);

            //CreateWaterSurface();
        }

        private void Init2plus()
        {
            //InitMapCoords3d_l();
            InitMapCoords1M();

            //_chart.ChartBackground.Color = Colors.DimGray;
            //_chart.ChartBackground.GradientColor = Colors.Black;
            //_chart.ChartBackground.GradientFill = GradientFill.Radial;



            var lights = View3D.CreateDefaultLights();

            //_chart.View3D.Lights = View3D.CreateDefaultLights();

            {
                _chart.View3D.SetPredefinedLightingScheme(LightingScheme.DiscoCMY);

                _chart.View3D.Lights[0].AmbientColor = System.Windows.Media.Color.FromArgb(255, 0, 0, 0);
                //_chart.View3D.Lights[0].AmbientColor = System.Windows.Media.Color.FromArgb(255, 255, 255, 255);
                _chart.View3D.Lights[0].AttenuationConstant = 0.5;
                _chart.View3D.Lights[0].AttenuationLinear = 0.1;
                _chart.View3D.Lights[0].AttenuationQuadratic = 0.05;
                // _chart.View3D.Lights[0].DiffuseColor = System.Windows.Media.Color.FromArgb(255, 211, 211, 211);
                _chart.View3D.Lights[0].DiffuseColor = System.Windows.Media.Color.FromArgb(255, 255, 255, 255);
                _chart.View3D.Lights[0].SpecularColor = System.Windows.Media.Color.FromArgb(255, 128, 128, 128);

                _chart.View3D.Lights[0].Type = LightType.Directional;
                _chart.View3D.Lights[0].Location.SetValues(-30, 50, -30);
                _chart.View3D.Lights[0].Target.SetValues(0, 0, 0);


                _chart.View3D.Lights[1].AmbientColor = System.Windows.Media.Color.FromArgb(255, 0, 0, 0);
                //_chart.View3D.Lights[1].AmbientColor = System.Windows.Media.Color.FromArgb(255, 255, 255, 255);
                //_chart.View3D.Lights[1].AttenuationConstant = 0.5;
                //_chart.View3D.Lights[1].AttenuationLinear = 0.001;
                //_chart.View3D.Lights[1].AttenuationQuadratic = 0.0003;
                //_chart.View3D.Lights[1].DiffuseColor = System.Windows.Media.Color.FromArgb(255, 211, 211, 211);
                _chart.View3D.Lights[1].DiffuseColor = System.Windows.Media.Color.FromArgb(255, 255, 255, 255);
                _chart.View3D.Lights[1].SpecularColor = System.Windows.Media.Color.FromArgb(255, 128, 128, 128);

                _chart.View3D.Lights[2].AmbientColor = System.Windows.Media.Color.FromArgb(255, 0, 0, 0);
                //_chart.View3D.Lights[2].AmbientColor = System.Windows.Media.Color.FromArgb(255, 255, 255, 255);
                // _chart.View3D.Lights[2].AttenuationConstant = 0.5;
                //_chart.View3D.Lights[2].AttenuationLinear = 0.001;
                //_chart.View3D.Lights[2].AttenuationQuadratic = 0.0003;
                // _chart.View3D.Lights[2].DiffuseColor = System.Windows.Media.Color.FromArgb(255, 211, 211, 211);
                _chart.View3D.Lights[2].DiffuseColor = System.Windows.Media.Color.FromArgb(255, 255, 255, 255);
                _chart.View3D.Lights[2].SpecularColor = System.Windows.Media.Color.FromArgb(255, 128, 128, 128);
            }

            {
                //_chart.View3D.Lights[0].AmbientColor = System.Windows.Media.Color.FromArgb(255, 0, 0, 0);
                //_chart.View3D.Lights[0].AttenuationConstant = 0.5;
                //_chart.View3D.Lights[0].AttenuationLinear = 0.1;
                //_chart.View3D.Lights[0].AttenuationQuadratic = 0.05;
                //_chart.View3D.Lights[0].DiffuseColor = System.Windows.Media.Color.FromArgb(255, 211, 211, 211);
                //_chart.View3D.Lights[0].SpecularColor = System.Windows.Media.Color.FromArgb(255, 128, 128, 128);

                //_chart.View3D.Lights[1].AmbientColor = System.Windows.Media.Color.FromArgb(255, 0, 0, 0);
                //_chart.View3D.Lights[1].AttenuationConstant = 0.5;
                //_chart.View3D.Lights[1].AttenuationLinear = 0.001;
                //_chart.View3D.Lights[1].AttenuationQuadratic = 0.0003;
                //_chart.View3D.Lights[1].DiffuseColor = System.Windows.Media.Color.FromArgb(255, 211, 211, 211);
                //_chart.View3D.Lights[1].SpecularColor = System.Windows.Media.Color.FromArgb(255, 128, 128, 128);
            }

            {

                //_chart.View3D.SetPredefinedLightingScheme(LightingScheme.Default);

                // _chart.View3D.SetPredefinedLightingScheme(LightingScheme.DiscoCMY);

                // _chart.View3D.Lights[0].AmbientColor = System.Windows.Media.Color.FromArgb(255, 0, 0, 0);
                // //_chart.View3D.Lights[0].AmbientColor = System.Windows.Media.Color.FromArgb(255, 255, 255, 255);
                // //_chart.View3D.Lights[0].AttenuationConstant = 0.5;
                // //_chart.View3D.Lights[0].AttenuationLinear = 0.1;
                // //_chart.View3D.Lights[0].AttenuationQuadratic = 0.05;
                //// _chart.View3D.Lights[0].DiffuseColor = System.Windows.Media.Color.FromArgb(255, 211, 211, 211);
                // _chart.View3D.Lights[0].DiffuseColor = System.Windows.Media.Color.FromArgb(255, 255, 255, 255);
                // _chart.View3D.Lights[0].SpecularColor = System.Windows.Media.Color.FromArgb(255, 128, 128, 128);

                // _chart.View3D.Lights[1].AmbientColor = System.Windows.Media.Color.FromArgb(255, 0, 0, 0);
                // //_chart.View3D.Lights[1].AmbientColor = System.Windows.Media.Color.FromArgb(255, 255, 255, 255);
                // //_chart.View3D.Lights[1].AttenuationConstant = 0.5;
                // //_chart.View3D.Lights[1].AttenuationLinear = 0.001;
                // //_chart.View3D.Lights[1].AttenuationQuadratic = 0.0003;
                // //_chart.View3D.Lights[1].DiffuseColor = System.Windows.Media.Color.FromArgb(255, 211, 211, 211);
                // _chart.View3D.Lights[1].DiffuseColor = System.Windows.Media.Color.FromArgb(255, 255, 255, 255);
                // _chart.View3D.Lights[1].SpecularColor = System.Windows.Media.Color.FromArgb(255, 128, 128, 128);

                // _chart.View3D.Lights[2].AmbientColor = System.Windows.Media.Color.FromArgb(255, 0, 0, 0);
                // //_chart.View3D.Lights[2].AmbientColor = System.Windows.Media.Color.FromArgb(255, 255, 255, 255);
                // // _chart.View3D.Lights[2].AttenuationConstant = 0.5;
                // //_chart.View3D.Lights[2].AttenuationLinear = 0.001;
                // //_chart.View3D.Lights[2].AttenuationQuadratic = 0.0003;
                // // _chart.View3D.Lights[2].DiffuseColor = System.Windows.Media.Color.FromArgb(255, 211, 211, 211);
                // _chart.View3D.Lights[2].DiffuseColor = System.Windows.Media.Color.FromArgb(255, 255, 255, 255);
                // _chart.View3D.Lights[2].SpecularColor = System.Windows.Media.Color.FromArgb(255, 128, 128, 128);


                //Console.WriteLine(_chart.View3D.Lights[0].Type);
                //Console.WriteLine($"_chart.View3D.Lights[0].Location.X: { _chart.View3D.Lights[0].Location.X}  _chart.View3D.Lights[0].Location.Y: {_chart.View3D.Lights[0].Location.Y} _chart.View3D.Lights[0].Location.Z: {_chart.View3D.Lights[0].Location.Z} ");
                //Console.WriteLine($"_chart.View3D.Lights[0].Target.X: { _chart.View3D.Lights[0].Target.X}  _chart.View3D.Lights[0].Target.Y: {_chart.View3D.Lights[0].Target.Y} _chart.View3D.Lights[0].Location.Z: {_chart.View3D.Lights[0].Target.Z} ");

                //Console.WriteLine(_chart.View3D.Lights[1].Type);
                //Console.WriteLine($"_chart.View3D.Lights[1].Location.X: { _chart.View3D.Lights[1].Location.X}  _chart.View3D.Lights[1].Location.Y: {_chart.View3D.Lights[1].Location.Y} _chart.View3D.Lights[1].Location.Z: {_chart.View3D.Lights[1].Location.Z} ");
                //Console.WriteLine($"_chart.View3D.Lights[1].Target.X: { _chart.View3D.Lights[1].Target.X}  _chart.View3D.Lights[1].Target.Y: {_chart.View3D.Lights[1].Target.Y} _chart.View3D.Lights[1].Location.Z: {_chart.View3D.Lights[1].Target.Z} ");

                //_chart.View3D.Lights[0].Location.SetValues(-30, 50, -30);
                //_chart.View3D.Lights[0].Target.SetValues(0, 0, 0);
                //_chart.View3D.Lights[1].Location.SetValues(30, 50, -30);
                //_chart.View3D.Lights[1].Target.SetValues(0, 0, 0);

                Console.WriteLine(_chart.View3D.Lights[0].Type);
                //Console.WriteLine($"_chart.View3D.Lights[0].Location.X: { _chart.View3D.Lights[0].Location.X}  _chart.View3D.Lights[0].Location.Y: {_chart.View3D.Lights[0].Location.Y} _chart.View3D.Lights[0].Location.Z: {_chart.View3D.Lights[0].Location.Z} ");
                //Console.WriteLine($"_chart.View3D.Lights[0].Target.X: { _chart.View3D.Lights[0].Target.X}  _chart.View3D.Lights[0].Target.Y: {_chart.View3D.Lights[0].Target.Y} _chart.View3D.Lights[0].Location.Z: {_chart.View3D.Lights[0].Target.Z} ");

                Console.WriteLine(_chart.View3D.Lights[1].Type);
                //Console.WriteLine($"_chart.View3D.Lights[1].Location.X: { _chart.View3D.Lights[1].Location.X}  _chart.View3D.Lights[1].Location.Y: {_chart.View3D.Lights[1].Location.Y} _chart.View3D.Lights[1].Location.Z: {_chart.View3D.Lights[1].Location.Z} ");
                //Console.WriteLine($"_chart.View3D.Lights[1].Target.X: { _chart.View3D.Lights[1].Target.X}  _chart.View3D.Lights[1].Target.Y: {_chart.View3D.Lights[1].Target.Y} _chart.View3D.Lights[1].Location.Z: {_chart.View3D.Lights[1].Target.Z} ");

                //_chart.View3D.Lights[0] = lights[0];
                //_chart.View3D.Lights[1] = lights[1];

                Console.WriteLine(_chart.View3D.Lights[2].Type);
                //Console.WriteLine($"_chart.View3D.Lights[2].Location.X: { _chart.View3D.Lights[2].Location.X}  _chart.View3D.Lights[2].Location.Y: {_chart.View3D.Lights[2].Location.Y} _chart.View3D.Lights[2].Location.Z: {_chart.View3D.Lights[2].Location.Z} ");
                //Console.WriteLine($"_chart.View3D.Lights[2].Target.X: { _chart.View3D.Lights[2].Target.X}  _chart.View3D.Lights[2].Target.Y: {_chart.View3D.Lights[2].Target.Y} _chart.View3D.Lights[2].Location.Z: {_chart.View3D.Lights[2].Target.Z} ");

            }

            //X axis shows latitude 
            {
                _chart.View3D.XAxisPrimary3D.SetRange(m_mapCoordCorner.Latitude, m_mapCoordMaxLat.Latitude);

                _chart.View3D.XAxisPrimary3D.Reversed = true;

                //_chart.View3D.XAxisPrimary3D.ValueType = AxisValueType.MapCoordsDegMinSecNESW;

                _chart.View3D.XAxisPrimary3D.ValueType = AxisValueType.MapCoordsDegNESW;
                //_chart.View3D.XAxisPrimary3D.ValueType = AxisValueType.MapCoordsDegPadMinSecNESW;
                //_chart.View3D.XAxisPrimary3D.ValueType = AxisValueType.MapCoordsDegrees;


                _chart.View3D.XAxisPrimary3D.Title.Text = "Latitude";
                _chart.View3D.XAxisPrimary3D.LabelsColor = Colors.White;
                _chart.View3D.XAxisPrimary3D.Title.Color = Colors.LightCyan;
                _chart.View3D.XAxisPrimary3D.LabelTicksGap = 20;

                _chart.View3D.XAxisPrimary3D.MouseDragSnapToDiv = false;
                _chart.View3D.XAxisPrimary3D.ScaleNibs = new Arction.Wpf.SemibindableCharting.Axes.AxisDragNib3D();

                _chart.View3D.XAxisPrimary3D.MouseInteraction = false;
                _chart.View3D.XAxisPrimary3D.MouseDragSnapToDiv = false;
                _chart.View3D.XAxisPrimary3D.MouseScaling = false;
                _chart.View3D.XAxisPrimary3D.MouseScrolling = false;
            }

            //Y axis shows the depth
            {
                _chart.View3D.YAxisPrimary3D.SetRange(0, 200);
                _chart.View3D.YAxisPrimary3D.Title.Text = "Height";
                _chart.View3D.YAxisPrimary3D.LabelsColor = Colors.White;
                _chart.View3D.YAxisPrimary3D.Title.Color = Colors.LightCyan;
                _chart.View3D.YAxisPrimary3D.Units.Text = "m";
                _chart.View3D.YAxisPrimary3D.LabelTicksGap = 20;

                _chart.View3D.YAxisPrimary3D.MouseDragSnapToDiv = false;

                _chart.View3D.YAxisPrimary3D.MouseInteraction = false;
                _chart.View3D.YAxisPrimary3D.MouseDragSnapToDiv = false;
                _chart.View3D.YAxisPrimary3D.MouseScaling = false;
                _chart.View3D.YAxisPrimary3D.MouseScrolling = false;
            }

            //Z axis shows longitude
            {
                _chart.View3D.ZAxisPrimary3D.SetRange(m_mapCoordCorner.Longitude, m_mapCoordMaxLon.Longitude);

                //_chart.View3D.ZAxisPrimary3D.ValueType = AxisValueType.MapCoordsDegMinSecNESW;

                _chart.View3D.ZAxisPrimary3D.ValueType = AxisValueType.MapCoordsDegNESW;
                //_chart.View3D.ZAxisPrimary3D.ValueType = AxisValueType.MapCoordsDegPadMinSecNESW;
                //_chart.View3D.ZAxisPrimary3D.ValueType = AxisValueType.MapCoordsDegrees;


                _chart.View3D.ZAxisPrimary3D.Title.Text = "Longitude";
                _chart.View3D.ZAxisPrimary3D.LabelsColor = Colors.White;
                _chart.View3D.ZAxisPrimary3D.Title.Color = Colors.LightCyan;
                _chart.View3D.ZAxisPrimary3D.LabelTicksGap = 20;

                _chart.View3D.ZAxisPrimary3D.MouseDragSnapToDiv = false;

                _chart.View3D.ZAxisPrimary3D.MouseInteraction = false;
                _chart.View3D.ZAxisPrimary3D.MouseDragSnapToDiv = false;
                _chart.View3D.ZAxisPrimary3D.MouseScaling = false;
                _chart.View3D.ZAxisPrimary3D.MouseScrolling = false;
            }

            //Hide walls 
            List<WallBase> walls = _chart.View3D.GetWalls();
            foreach (WallBase wall in walls)
            {
                wall.Visible = false;
            }

            Bitmap bitmap = (Bitmap)ChartTools.ImageFromResource("Resources.SeaBottom.jpg", Assembly.GetExecutingAssembly());
            //CreateSeaBottomSurface(bitmap);
            //CreateSeaBottomSurface2(bitmap);

            //CreateWaterSurface();

        }

        private void Init3()
        {
            //_chart.Width = 3000;
            //_chart.Height = 1000;

            _chart.ChartBackground.Color = Colors.DimGray;
            _chart.ChartBackground.GradientColor = Colors.Black;
            _chart.ChartBackground.GradientFill = GradientFill.Radial;

            _chart.View3D.Lights = View3D.CreateDefaultLights();

            //_chart.View3D.Lights[1].Location.SetValues(60, 50, 80);


            //X axis shows longitude
            _chart.View3D.XAxisPrimary3D.SetRange(0, 3000);
            _chart.View3D.XAxisPrimary3D.Title.Text = "X";
            _chart.View3D.XAxisPrimary3D.LabelsColor = Colors.White;
            _chart.View3D.XAxisPrimary3D.Title.Color = Colors.LightCyan;
            _chart.View3D.XAxisPrimary3D.LabelTicksGap = 20;

            _chart.View3D.XAxisPrimary3D.MouseInteraction = false;

            //Y axis shows the depth
            _chart.View3D.YAxisPrimary3D.SetRange(0, 1000);
            _chart.View3D.YAxisPrimary3D.Title.Text = "Height";
            _chart.View3D.YAxisPrimary3D.LabelsColor = Colors.White;
            _chart.View3D.YAxisPrimary3D.Title.Color = Colors.LightCyan;
            _chart.View3D.YAxisPrimary3D.Units.Text = "m";
            _chart.View3D.YAxisPrimary3D.LabelTicksGap = 20;

            _chart.View3D.YAxisPrimary3D.MouseInteraction = false;

            //Z axis shows latitude
            _chart.View3D.ZAxisPrimary3D.SetRange(0, 3000);
            _chart.View3D.ZAxisPrimary3D.Title.Text = "Z";
            _chart.View3D.ZAxisPrimary3D.LabelsColor = Colors.White;
            _chart.View3D.ZAxisPrimary3D.Title.Color = Colors.LightCyan;
            _chart.View3D.ZAxisPrimary3D.LabelTicksGap = 20;

            _chart.View3D.ZAxisPrimary3D.MouseInteraction = false;


            //Hide walls 
            List<WallBase> walls = _chart.View3D.GetWalls();
            foreach (WallBase wall in walls)
            {
                wall.Visible = false;
            }

            Bitmap bitmap = (Bitmap)ChartTools.ImageFromResource("Resources.SeaBottom.jpg", Assembly.GetExecutingAssembly());
            //CreateSeaBottomSurface(bitmap);

            //CreateWaterSurface();
        }

        private void CreateSeaBottomSurface(Bitmap bitmap)
        {
            SurfaceGridSeries3D seaBottom = new SurfaceGridSeries3D(_chart.View3D, Axis3DBinding.Primary,
                Axis3DBinding.Primary, Axis3DBinding.Primary);
            seaBottom.WireframeType = SurfaceWireframeType3D.None;
            seaBottom.FadeAway = 0;
            seaBottom.ContourPalette.Type = PaletteType.Gradient;
            seaBottom.Title.Text = "Depth";
            seaBottom.FastContourZoneHeight = 3;
            _chart.View3D.SurfaceGridSeries3D.Add(seaBottom);

            ValueRangePalette palette = new ValueRangePalette(seaBottom);
            //ExampleUtils.DisposeAllAndClear(palette.Steps);

            palette.MinValue = 0;
            palette.Steps.Add(new PaletteStep(palette, Colors.Navy, 200));
            palette.Steps.Add(new PaletteStep(palette, Colors.Lime, 400));
            palette.Steps.Add(new PaletteStep(palette, Colors.Yellow, 600));
            palette.Steps.Add(new PaletteStep(palette, Colors.Red, 800));
            palette.Steps.Add(new PaletteStep(palette, Colors.Gray, 1000));
            seaBottom.ContourPalette = palette;

            BitmapAntialiasOptions options = new BitmapAntialiasOptions();

            if (seaBottom.SetHeightDataFromBitmap(_chart.View3D.XAxisPrimary3D.Minimum, _chart.View3D.XAxisPrimary3D.Maximum,
                 _chart.View3D.YAxisPrimary3D.Maximum, _chart.View3D.YAxisPrimary3D.Minimum,
                _chart.View3D.ZAxisPrimary3D.Minimum, _chart.View3D.ZAxisPrimary3D.Maximum,
                bitmap, options, false) == false)
            {
                MessageBox.Show("Surface data creation failed.");
            }
        }

        private void CreateSeaBottomSurface2(Bitmap bitmap)
        {
            SurfaceGridSeries3D seaBottom = new SurfaceGridSeries3D(_chart.View3D, Axis3DBinding.Primary,
                Axis3DBinding.Primary, Axis3DBinding.Primary);
            seaBottom.WireframeType = SurfaceWireframeType3D.None;
            seaBottom.FadeAway = 0;
            seaBottom.ContourPalette.Type = PaletteType.Gradient;
            seaBottom.Title.Text = "Depth";
            seaBottom.FastContourZoneHeight = 3;
            _chart.View3D.SurfaceGridSeries3D.Clear();
            _chart.View3D.SurfaceGridSeries3D.Add(seaBottom);

            seaBottom.ShowInLegendBox = false;

            ValueRangePalette palette = new ValueRangePalette(seaBottom);
            //ExampleUtils.DisposeAllAndClear(palette.Steps);

            palette.MinValue = 0;
            palette.Steps.Add(new PaletteStep(palette, Colors.Green, 0));
            palette.Steps.Add(new PaletteStep(palette, Colors.Lime, 100));
            palette.ShowMinValueLegendLabel = false;
            seaBottom.ContourPalette = palette;


            BitmapAntialiasOptions options = new BitmapAntialiasOptions();

            if (seaBottom.SetHeightDataFromBitmap(_chart.View3D.XAxisPrimary3D.Minimum, _chart.View3D.XAxisPrimary3D.Maximum,
                 _chart.View3D.YAxisPrimary3D.Maximum, _chart.View3D.YAxisPrimary3D.Minimum,
                _chart.View3D.ZAxisPrimary3D.Minimum, _chart.View3D.ZAxisPrimary3D.Maximum,
                bitmap, options, false) == false)
            {
                MessageBox.Show("Surface data creation failed.");
            }
        }

        private void CreateWaterSurface()
        {
            SurfaceGridSeries3D water = new SurfaceGridSeries3D(_chart.View3D, Axis3DBinding.Primary,
                Axis3DBinding.Primary, Axis3DBinding.Primary);
            water.WireframeType = SurfaceWireframeType3D.None;
            water.Fill = SurfaceFillStyle.Toned;
            //water.BaseColor = System.Windows.Media.Color.FromArgb(50, 0, 255, 255);
            //water.ToneColor = System.Windows.Media.Color.FromArgb(50, 50, 50, 50);
            water.BaseColor = System.Windows.Media.Color.FromArgb(255, 50, 185, 65);
            water.ToneColor = System.Windows.Media.Color.FromArgb(255, 0, 255, 25);

            //water.BaseColor = Colors.Transparent;
            //water.ToneColor = Colors.Transparent;

            water.MouseInteraction = true;

            water.MouseClick += Water_MouseClick;
            water.MouseTraceCellChanged += Water_MouseTraceCellChanged;

            water.ColorSaturation = 50;
            water.ContourLineType = ContourLineType3D.None;
            water.InitialValue = 0;
            water.SetSize(50, 50);
            water.SetRangesXZ(_chart.View3D.XAxisPrimary3D.Minimum, _chart.View3D.XAxisPrimary3D.Maximum,
                _chart.View3D.ZAxisPrimary3D.Minimum, _chart.View3D.ZAxisPrimary3D.Maximum);

            //Don't show the water plane in the legend box 
            water.ShowInLegendBox = false;

            _chart.View3D.SurfaceGridSeries3D.Add(water);
        }

        private void CreateWaterSurface2()
        {
            SurfaceGridSeries3D water = new SurfaceGridSeries3D(_chart.View3D, Axis3DBinding.Primary,
                Axis3DBinding.Primary, Axis3DBinding.Primary);
            water.WireframeType = SurfaceWireframeType3D.None;
            water.Fill = SurfaceFillStyle.Toned;
            //water.BaseColor = System.Windows.Media.Color.FromArgb(50, 0, 255, 255);
            //water.ToneColor = System.Windows.Media.Color.FromArgb(50, 50, 50, 50);
            water.BaseColor = System.Windows.Media.Color.FromArgb(255, 50, 185, 65);
            water.ToneColor = System.Windows.Media.Color.FromArgb(255, 0, 255, 25);

            //water.BaseColor = Colors.Transparent;
            //water.ToneColor = Colors.Transparent;

            water.MouseInteraction = true;

            water.MouseClick += Water_MouseClick;
            water.MouseTraceCellChanged += Water_MouseTraceCellChanged;

            water.ColorSaturation = 50;
            water.ContourLineType = ContourLineType3D.None;
            water.InitialValue = 0;
            water.SetSize(50, 50);

            water.SetRangesXZ(_chart.View3D.XAxisPrimary3D.Minimum, _chart.View3D.XAxisPrimary3D.Maximum,
                _chart.View3D.ZAxisPrimary3D.Minimum, _chart.View3D.ZAxisPrimary3D.Maximum);

            //Don't show the water plane in the legend box 
            water.ShowInLegendBox = false;

            _chart.View3D.SurfaceGridSeries3D.Add(water);
        }

        private void InitRectangles()
        {
            double centerX = (_chart.View3D.XAxisPrimary3D.Minimum + _chart.View3D.XAxisPrimary3D.Maximum) / 2.0;
            double centerY = (_chart.View3D.YAxisPrimary3D.Minimum + _chart.View3D.YAxisPrimary3D.Maximum) / 2.0;
            double dCenterZ = (_chart.View3D.ZAxisPrimary3D.Minimum + _chart.View3D.ZAxisPrimary3D.Maximum) / 2.0;

            var m_planeX = new Rectangle3D(_chart.View3D, Axis3DBinding.Primary, Axis3DBinding.Primary, Axis3DBinding.Primary);
            m_planeX.Center.SetValues(_chart.View3D.XAxisPrimary3D.Minimum, centerY, dCenterZ);
            m_planeX.Size.Height = _chart.View3D.Dimensions.Z;
            m_planeX.Size.Width = _chart.View3D.Dimensions.Y;
            m_planeX.Fill.UseImage = false;
            m_planeX.Fill.Material.DiffuseColor = System.Windows.Media.Color.FromArgb(150, 255, 0, 0);
            m_planeX.Rotation.SetValues(0, 0, -90);
            m_planeX.MouseInteraction = true;
            _chart.View3D.Rectangles.Add(m_planeX);

            m_planeX.MouseClick += M_planeX_MouseClick;



            var m_planeZ = new Rectangle3D(_chart.View3D, Axis3DBinding.Primary, Axis3DBinding.Primary, Axis3DBinding.Primary);
            m_planeZ.Center.SetValues(centerX, centerY, _chart.View3D.ZAxisPrimary3D.Maximum);
            m_planeZ.Size.Height = _chart.View3D.Dimensions.Y;
            m_planeZ.Size.Width = _chart.View3D.Dimensions.Z;
            m_planeZ.Fill.UseImage = false;
            m_planeZ.Fill.Material.DiffuseColor = System.Windows.Media.Color.FromArgb(150, 0, 0, 255);
            m_planeZ.Rotation.SetValues(-90, 0, 0);
            m_planeZ.MouseInteraction = false;
            _chart.View3D.Rectangles.Add(m_planeZ);


        }

        private void M_planeX_MouseClick(object sender, MouseEventArgs e)
        {
            var p = e.GetPosition(_chart);
        }

        private void Water_MouseTraceCellChanged(object sender, MouseTraceCellChangedEventArgs e)
        {
            var _surface = _chart.View3D.SurfaceGridSeries3D[0];

            //Move tracing point 
            double dXGridStep = (_surface.RangeMaxX - _surface.RangeMinX) / (double)(_surface.SizeX - 1);
            double zGridStep = (_surface.RangeMaxZ - _surface.RangeMinZ) / (double)(_surface.SizeZ - 1);
            var TargetAxisValues_X = _surface.RangeMinX + dXGridStep * (double)e.NewColumn + dXGridStep / 2.0;
            var TargetAxisValues_Z = _surface.RangeMinZ + zGridStep * (double)e.NewRow + zGridStep / 2.0;
            //Calculate average of cell corner Y values 
            var TargetAxisValues_Y = (_surface.Data[e.NewColumn, e.NewRow].Y + _surface.Data[e.NewColumn, e.NewRow + 1].Y
                + _surface.Data[e.NewColumn + 1, e.NewRow].Y + _surface.Data[e.NewColumn + 1, e.NewRow + 1].Y) / 4.0;

            var m_mouseTrackLabel_Text = string.Format("Cell [{0},{1}]\nY={2}", e.NewColumn, e.NewRow, TargetAxisValues_Y.ToString("0.0"));

            var Center_X = TargetAxisValues_X;

            var Center_Z = TargetAxisValues_Z;

            Console.WriteLine(Center_X.ToString() + " " + Center_Z.ToString());

        }

        private void Water_MouseClick(object sender, MouseEventArgs e)
        {
            var point = e.GetPosition(_chart);
            var p = Mouse.GetPosition(_chart);

            //var point1 = e.GetPosition(_chart.View3D.SurfaceGridSeries3D[0]);

            //_chart.View3D.SurfaceGridSeries3D[0].

            //_chart.View3D.Convert3DWorldCoordsToScreenCoords

        }

        MeshModel DroneModel;
        MeshModel DroneModelRDM;
        MeshModel PalaceModel;

        private void LoadModelsFromResources()
        {

            Assembly assembly = Assembly.GetExecutingAssembly();

            //Load ship from resource stream 
            //string strResourceStream = assembly.GetName().Name + "." + "Resources.Destroyer.obj";

            string strResourceStream = assembly.GetName().Name + "." + "Resources.Drone.obj";
            //strResourceStream = assembly.GetName().Name + "." + "Resources.Submarine.obj";

            //string path = "C:\\Users\\User\\Documents\\visual studio 2017\\Projects\\try_3D\\try_3D\\Resources\\Drone.obj";

            DroneModel = new MeshModel(_chart.View3D, Axis3DBinding.Primary, Axis3DBinding.Primary, Axis3DBinding.Primary);
            //DroneModel.LoadFromFile(path);
            DroneModel.LoadFromResource(strResourceStream);
            DroneModel.Position.X = 150;
            DroneModel.Position.Y = 150;
            DroneModel.Position.Z = 150;
            DroneModel.MouseInteraction = true;
            DroneModel.Rotation.SetValues(0, 0, 0);
            DroneModel.Size.SetValues(0.5f, 0.5f, 0.5f);
            _chart.View3D.MeshModels.Add(DroneModel);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();

            string strResourceStream = assembly.GetName().Name + "." + "Resources.Drone.obj";

            DroneModel = new MeshModel(
                _chart.View3D,
                Axis3DBinding.Primary,
                Axis3DBinding.Primary,
                Axis3DBinding.Primary);
            DroneModel.LoadFromResource(strResourceStream);
            DroneModel.Position.X = 150;
            DroneModel.Position.Y = 150;
            DroneModel.Position.Z = 150;
            DroneModel.MouseInteraction = true;
            DroneModel.Rotation.SetValues(0, 0, 0);
            DroneModel.Size.SetValues(0.2f, 0.2f, 0.2f);
            _chart.View3D.MeshModels.Add(DroneModel);
        }

        private void Button15_Click(object sender, RoutedEventArgs e)
        {
            //MyTask();
            //await Task.Run(() => MyTask());
            //Task.Run(() => MyTask());
            //await Task.Factory.StartNew(() => MyTask(), TaskCreationOptions.LongRunning);
            //Task.Run(() => MyTask2());
            //Console.WriteLine("Button15_Click");
            //Dispatcher.Invoke(() => MyTask0());
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, new Action(() => MyTask0()));
        }


        private async void MyTask0()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();

            string strResourceStream = assembly.GetName().Name + "." + "Resources.Drone.obj";

            DroneModel = new MeshModel(
                _chart.View3D,
                Axis3DBinding.Primary,
                Axis3DBinding.Primary,
                Axis3DBinding.Primary);
            DroneModel.LoadFromResource(strResourceStream);
            DroneModel.Position.X = 150;
            DroneModel.Position.Y = 150;
            DroneModel.Position.Z = 150;
            DroneModel.MouseInteraction = true;
            DroneModel.Rotation.SetValues(0, 0, 0);
            DroneModel.Size.SetValues(0.2f, 0.2f, 0.2f);
            _chart.View3D.MeshModels.Add(DroneModel);
        }

        private void MyTask()
        {
            if (!CheckAccess())
            {
                // On a different thread
                Dispatcher.Invoke(() => MyTask());
                return;
            }

            Assembly assembly = Assembly.GetExecutingAssembly();

            string strResourceStream = assembly.GetName().Name + "." + "Resources.Drone.obj";

            DroneModel = new MeshModel(
                _chart.View3D,
                Axis3DBinding.Primary,
                Axis3DBinding.Primary,
                Axis3DBinding.Primary);
            DroneModel.LoadFromResource(strResourceStream);
            DroneModel.Position.X = 150;
            DroneModel.Position.Y = 150;
            DroneModel.Position.Z = 150;
            DroneModel.MouseInteraction = true;
            DroneModel.Rotation.SetValues(0, 0, 0);
            DroneModel.Size.SetValues(0.5f, 0.5f, 0.5f);
            _chart.View3D.MeshModels.Add(DroneModel);
        }

        private async void MyTask2()
        {
            await Task.Delay(1);
            if (!CheckAccess())
            {
                // On a different thread
                Dispatcher.Invoke(() => MyTask2());
                return;
            }

            Assembly assembly = Assembly.GetExecutingAssembly();

            string strResourceStream = assembly.GetName().Name + "." + "Resources.Drone.obj";

            DroneModel = new MeshModel(
                _chart.View3D,
                Axis3DBinding.Primary,
                Axis3DBinding.Primary,
                Axis3DBinding.Primary);
            DroneModel.LoadFromResource(strResourceStream);
            DroneModel.Position.X = 150;
            DroneModel.Position.Y = 150;
            DroneModel.Position.Z = 150;
            DroneModel.MouseInteraction = true;
            DroneModel.Rotation.SetValues(0, 0, 0);
            DroneModel.Size.SetValues(0.5f, 0.5f, 0.5f);
            _chart.View3D.MeshModels.Add(DroneModel);
        }

        private void Button2_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            DroneModel.Position.X = random.Next(3000);
            DroneModel.Position.Y = random.Next(1000);
            DroneModel.Position.Z = random.Next(3000);
        }



        private bool flag = false;

        System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
        private void InitTimer()
        {
            dispatcherTimer.Tick += dispatcherTimer_Tick;
            //dispatcherTimer.Tick += dispatcherTimer_Tick2;
            //dispatcherTimer.Tick += dispatcherTimer_Tick3;

            //dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 10);

            dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 20);
        }

        private void Button25_Click(object sender, RoutedEventArgs e)
        {
            //DroneModel.Position.X =1500;
            //DroneModel.Position.Y = 500;
            //DroneModel.Position.Z = 500;
            flag = !flag;
            if (flag)
            {
                dispatcherTimer.Start(); //работает
                //await Task.Run(() => Circle2());
                //Task.Run(() => Circle2());
                //Dispatcher.Invoke(() => Circle0());
                //Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, new Action(() => Circle0()));
                //Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, new Action(() => Circle00()));

                //Task.Factory.StartNew<Task>(() => Circle(), TaskCreationOptions.LongRunning);
            }
            else
            {
                dispatcherTimer.Stop();
                //point = 0.0;
            }
            //Console.WriteLine("Button25_Click");
        }


        private void Circle0()
        {
            while (flag)
            {
                Random random = new Random();

                point += 1.0;

                double x = Math.Cos(2 * Math.PI * point / 360) * 1000 + 1500;
                double z = Math.Sin(2 * Math.PI * point / 360) * 1000 + 1500;
                double y = Math.Cos(2 * Math.PI * point / 360) * 250 + 500;

                //DroneModel.Position.X = (float)x;
                //DroneModel.Position.Y = (float)y;
                //DroneModel.Position.Z = (float)z;

                Dispatcher.Invoke(() => DroneModel.Position.X = (float)x);
                Dispatcher.Invoke(() => DroneModel.Position.Y = (float)y);
                Dispatcher.Invoke(() => DroneModel.Position.Z = (float)z);

                Thread.Sleep(10);
            }
        }
        private async Task Circle00()
        {
            while (flag)
            {
                Random random = new Random();

                point += 1.0;

                double x = Math.Cos(2 * Math.PI * point / 360) * 1000 + 1500;
                double z = Math.Sin(2 * Math.PI * point / 360) * 1000 + 1500;
                double y = Math.Cos(2 * Math.PI * point / 360) * 250 + 500;

                DroneModel.Position.X = (float)x;
                DroneModel.Position.Y = (float)y;
                DroneModel.Position.Z = (float)z;

                await Task.Delay(10);
            }
        }
        private async Task Circle()
        {
            while (flag)
            {
                Random random = new Random();

                point += 1.0;

                double x = Math.Cos(2 * Math.PI * point / 360) * 1000 + 1500;
                double z = Math.Sin(2 * Math.PI * point / 360) * 1000 + 1500;
                double y = Math.Cos(2 * Math.PI * point / 360) * 250 + 500;

                //DroneModel.Position.X = (float)x;
                //DroneModel.Position.Y = (float)y;
                //DroneModel.Position.Z = (float)z;

                Dispatcher.Invoke(() => DroneModel.Position.X = (float)x);
                Dispatcher.Invoke(() => DroneModel.Position.Y = (float)y);
                Dispatcher.Invoke(() => DroneModel.Position.Z = (float)z);

                //Console.WriteLine(point);
                await Task.Delay(10);
            }
        }
        private void Circle2()
        {
            if (!CheckAccess())
            {
                // On a different thread
                Dispatcher.Invoke(() => Circle2());
                return;
            }

            while (flag)
            {
                Random random = new Random();

                point += 1.0;

                double x = Math.Cos(2 * Math.PI * point / 360) * 1000 + 1500;
                double z = Math.Sin(2 * Math.PI * point / 360) * 1000 + 1500;
                double y = Math.Cos(2 * Math.PI * point / 360) * 250 + 500;

                DroneModel.Position.X = (float)x;
                DroneModel.Position.Y = (float)y;
                DroneModel.Position.Z = (float)z;

                //Dispatcher.Invoke(() => DroneModel.Position.X = (float)x);
                //Dispatcher.Invoke(() => DroneModel.Position.Y = (float)y);
                //Dispatcher.Invoke(() => DroneModel.Position.Z = (float)z);

                //await Task.Delay(10);
                Thread.Sleep(10);
            }
        }

        double point = 1.0;
        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            Random random = new Random();
            //DroneModel.Position.X = random.Next((int)DroneModel.Position.X - 1, (int)DroneModel.Position.X + 1);
            //DroneModel.Position.Y = random.Next((int)DroneModel.Position.Y - 1, (int)DroneModel.Position.Y + 1);
            //DroneModel.Position.Z = random.Next((int)DroneModel.Position.Z - 1, (int)DroneModel.Position.Z + 1);

            //point += 1.0;

            point += 2.0;

            if (point >= 360) point = point - 360;

            var xstart = _chart.View3D.XAxisPrimary3D.Minimum;
            var zstart = _chart.View3D.ZAxisPrimary3D.Minimum;

            var ystart = _chart.View3D.YAxisPrimary3D.Minimum;


            var xwidth = (float)(_chart.View3D.XAxisPrimary3D.Maximum - _chart.View3D.XAxisPrimary3D.Minimum);
            var zwidth = (float)(_chart.View3D.ZAxisPrimary3D.Maximum - _chart.View3D.ZAxisPrimary3D.Minimum);

            var ywidth = (float)(_chart.View3D.YAxisPrimary3D.Maximum - _chart.View3D.YAxisPrimary3D.Minimum);

            //double x = Math.Cos(2 * Math.PI * point / 360) * 1000 + 1500;
            //double z = Math.Sin(2 * Math.PI * point / 360) * 1000 + 1500;
            //double y = Math.Cos(2 * Math.PI * point / 360) * 250 + 500;

            double x = Math.Cos(2 * Math.PI * point / 360) * (xwidth / 3) + xstart + (xwidth / 2);
            double z = Math.Sin(2 * Math.PI * point / 360) * (zwidth / 3) + zstart + (zwidth / 2);
            double y = Math.Cos(2 * Math.PI * point / 360) * (ywidth / 4) + (ywidth / 2);



            DroneModel.Position.X = (float)x;
            DroneModel.Position.Y = (float)y;
            DroneModel.Position.Z = (float)z;

            _chart.View3D.Annotations[0].TargetAxisValues.SetValues(DroneModel.Position.X, DroneModel.Position.Y, DroneModel.Position.Z);
            //_chart.View3D.Annotations[0].Text = "X: " + DroneModel.Position.X.ToString("F0") + "\nY: " + DroneModel.Position.Y.ToString("F0") + "\nZ: " + DroneModel.Position.Z.ToString("F0");
            _chart.View3D.Annotations[0].Text = "X: " + DroneModel.Position.X.ToString("F6") + "\nY: " + DroneModel.Position.Y.ToString("F0") + "\nZ: " + DroneModel.Position.Z.ToString("F6");


            {
                //if (lseriesPoint3Ds.Count < 100)
                //{
                //    lseriesPoint3Ds.Add(new SeriesPoint3D(x, y, z));
                //}
                //else
                //{
                //    lseriesPoint3Ds.RemoveAt(0);
                //    lseriesPoint3Ds.Add(new SeriesPoint3D(x, y, z));
                //}

                //dataSeries.Points = lseriesPoint3Ds.ToArray();
            }


            if (lseriesPoint3Ds.Count < 150)
            {
                lseriesPoint3Ds.Add(new SeriesPoint3D(x, y, z));
            }
            else
            {
                lseriesPoint3Ds.RemoveAt(0);
                lseriesPoint3Ds.Add(new SeriesPoint3D(x, y, z));
            }


            if (lseries2Point3Ds.Count < 450)
            {
                lseries2Point3Ds.Add(new SeriesPoint3D(x, y, z));
                lseries2Point3Ds.Add(new SeriesPoint3D(x, 0, z));
                lseries2Point3Ds.Add(new SeriesPoint3D(x, y, z));
            }
            else
            {
                lseries2Point3Ds.RemoveAt(0);
                lseries2Point3Ds.RemoveAt(0);
                lseries2Point3Ds.RemoveAt(0);
                lseries2Point3Ds.Add(new SeriesPoint3D(x, y, z));
                lseries2Point3Ds.Add(new SeriesPoint3D(x, 0, z));
                lseries2Point3Ds.Add(new SeriesPoint3D(x, y, z));
            }

            dataSeries.Points = lseriesPoint3Ds.ToArray();

            dataSeries2.Points = lseries2Point3Ds.ToArray();
        }


        List<(double x, double y, double z)> lvts = new List<(double x, double y, double z)>();

        private List<(double x, double y, double z)> GetTupleCoords((double lat, double lon) latlon, (double sdgree, double edgree) dgrees, bool flag)
        {
            //var xstart = 53.930653;
            //var zstart = 27.634690;
            var xstart = latlon.lat;
            var zstart = latlon.lon;

            List<(double x, double y, double z)> result = new List<(double x, double y, double z)>();

            var ystart = _chart.View3D.YAxisPrimary3D.Minimum;
            var ywidth = (float)(_chart.View3D.YAxisPrimary3D.Maximum - _chart.View3D.YAxisPrimary3D.Minimum);

            if (flag)
            {
                var sdgree = dgrees.sdgree;
                while (sdgree != dgrees.edgree)
                {
                    (double x, double y, double z) temp;

                    //temp.x = Math.Cos(2 * Math.PI * temppoint / 360) * 0.0005 + xstart;
                    //temp.z = Math.Sin(2 * Math.PI * temppoint / 360) * 0.001 + zstart;
                    //temp.y = Math.Cos(2 * Math.PI * temppoint / 360) * (ywidth / 4) + (ywidth / 2);

                    temp.x = Math.Cos(2 * Math.PI * sdgree / 360) * 0.0005 + xstart;
                    temp.z = Math.Sin(2 * Math.PI * sdgree / 360) * 0.001 + zstart;
                    temp.y = Math.Cos(2 * Math.PI * sdgree / 360) * (ywidth / 4) + (ywidth / 2);

                    result.Add(temp);

                    sdgree = sdgree + 2.0;

                    if (sdgree >= 360)
                        sdgree = 0;
                }
            }
            else
            {
                var sdgree = dgrees.sdgree;
                while (sdgree != dgrees.edgree)
                {
                    (double x, double y, double z) temp;

                    temp.x = Math.Cos(2 * Math.PI * sdgree / 360) * 0.0005 + xstart;
                    temp.z = Math.Sin(2 * Math.PI * sdgree / 360) * 0.001 + zstart;
                    temp.y = Math.Cos(2 * Math.PI * sdgree / 360) * (ywidth / 4) + (ywidth / 2);

                    result.Add(temp);

                    sdgree = sdgree - 2.0;
                }
            }
            return result;
        }

        private void InitCoorsTupleList()
        {
            var v1 = GetTupleCoords((53.930653, 27.634690), (270, 90), true);
            lvts.AddRange(v1);

            var v2 = GetTupleCoords((53.930653, 27.636690), (270, 90), false);
            lvts.AddRange(v2);

            var v3 = GetTupleCoords((53.930653, 27.638690), (270, 90), true);
            lvts.AddRange(v3);

            var v4 = GetTupleCoords((53.930653, 27.640690), (270, 90), false);
            lvts.AddRange(v4);
        }

        private void InitCoorsTupleList2()
        {
            var v1 = GetTupleCoords((53.930553, 27.634784), (270, 90), false);
            lvts.AddRange(v1);

            var v2 = GetTupleCoords((53.930949, 27.636843), (270, 180), false);
            lvts.AddRange(v2);

            var v3 = GetTupleCoords((53.931473, 27.637461), (0, 180), true);
            lvts.AddRange(v3);


            var v4 = GenerateStraightTupleCoords((53.931754, 27.638037), (53.934987, 27.635657), true, false);
            lvts.AddRange(v4);

            var v5 = GenerateStraightTupleCoords((53.934987, 27.635657), (53.933798, 27.632091), false, true);
            lvts.AddRange(v5);

            var v6 = GenerateStraightTupleCoords((53.933798, 27.632091), (53.930385, 27.634166), true, false);
            lvts.AddRange(v6);

            //53.931754, 27.638037 

            //53.934987, 27.635657

            //53.933798, 27.632091

            //53.930385, 27.634166

        }

        private List<(double x, double y, double z)> GenerateStraightTupleCoords((double lat, double lon) latlon, (double lat, double lon) latlon2, bool f1, bool f2)
        {
            List<(double x, double y, double z)> result = new List<(double x, double y, double z)>();

            if (f1)
            {
                var w = (latlon2.lat - latlon.lat) / 100d;

                var xstart = latlon.lat;
                var xend = latlon2.lat;

                var ystart = _chart.View3D.YAxisPrimary3D.Minimum;
                var ywidth = (float)(_chart.View3D.YAxisPrimary3D.Maximum - _chart.View3D.YAxisPrimary3D.Minimum);

                Random r = new Random();

                while (xstart <= xend)
                {
                    (double x, double y, double z) temp;

                    temp.x = xstart; ;
                    temp.z = latlon.lon;
                    temp.y = Math.Cos(2 * Math.PI * (r.NextDouble() * 360) / 360) * (ywidth / 4) + (ywidth / 2);

                    result.Add(temp);

                    xstart = xstart + w;
                }
            }

            if (f2)
            {
                //var w = (latlon2.lon - latlon.lon) / 100d;
                var w = (latlon.lon - latlon2.lon) / 100d;

                var xstart = latlon.lon;
                var xend = latlon2.lon;

                var ystart = _chart.View3D.YAxisPrimary3D.Minimum;
                var ywidth = (float)(_chart.View3D.YAxisPrimary3D.Maximum - _chart.View3D.YAxisPrimary3D.Minimum);

                Random r = new Random();

                while (xstart >= xend)
                {
                    (double x, double y, double z) temp;

                    temp.x = latlon.lat;
                    temp.z = xstart;
                    temp.y = Math.Cos(2 * Math.PI * (r.NextDouble() * 360) / 360) * (ywidth / 4) + (ywidth / 2);

                    result.Add(temp);

                    xstart = xstart - w;
                }
            }

            return result;
        }



        private void getcoord(double temppoint, ref double x, ref double y, ref double z)
        {
            var xstart = 53.930653;
            var zstart = 27.634690;

            var ystart = _chart.View3D.YAxisPrimary3D.Minimum;
            var ywidth = (float)(_chart.View3D.YAxisPrimary3D.Maximum - _chart.View3D.YAxisPrimary3D.Minimum);

            x = Math.Cos(2 * Math.PI * temppoint / 360) * 0.0005 + xstart;
            z = Math.Sin(2 * Math.PI * temppoint / 360) * 0.001 + zstart;
            y = Math.Cos(2 * Math.PI * temppoint / 360) * (ywidth / 4) + (ywidth / 2);
        }

        private void getcoord2(double temppoint, ref double x, ref double y, ref double z)
        {
            var xstart = 53.930653;
            //var zstart = 27.634690;
            var zstart = 27.635690;

            var ystart = _chart.View3D.YAxisPrimary3D.Minimum;
            var ywidth = (float)(_chart.View3D.YAxisPrimary3D.Maximum - _chart.View3D.YAxisPrimary3D.Minimum);

            x = Math.Cos(2 * Math.PI * temppoint / 360) * 0.0005 + xstart;
            z = Math.Sin(2 * Math.PI * temppoint / 360) * 0.001 + zstart;
            y = Math.Cos(2 * Math.PI * temppoint / 360) * (ywidth / 4) + (ywidth / 2);
        }


        int listcount = 0;

        private void dispatcherTimer_Tick2(object sender, EventArgs e)
        {
            (double x, double y, double z) temp;

            if (listcount < lvts.Count)
            {
                temp = lvts[listcount];
                listcount++;
            }
            else
            {
                listcount = 0;
                temp = lvts[listcount];
            }

            double x = temp.x;
            double z = temp.z;
            double y = temp.y;

            DroneModel.Position.X = (float)x;
            DroneModel.Position.Y = (float)y;
            DroneModel.Position.Z = (float)z;

            _chart.View3D.Annotations[0].TargetAxisValues.SetValues(DroneModel.Position.X, DroneModel.Position.Y, DroneModel.Position.Z);
            //_chart.View3D.Annotations[0].Text = "X: " + DroneModel.Position.X.ToString("F0") + "\nY: " + DroneModel.Position.Y.ToString("F0") + "\nZ: " + DroneModel.Position.Z.ToString("F0");
            _chart.View3D.Annotations[0].Text = "X: " + DroneModel.Position.X.ToString("F6") + "\nY: " + DroneModel.Position.Y.ToString("F0") + "\nZ: " + DroneModel.Position.Z.ToString("F6");


            if (lseriesPoint3Ds.Count < 1500)
            {
                lseriesPoint3Ds.Add(new SeriesPoint3D(x, y, z));
            }
            else
            {
                lseriesPoint3Ds.RemoveAt(0);
                lseriesPoint3Ds.Add(new SeriesPoint3D(x, y, z));
            }


            if (lseries2Point3Ds.Count < 4500)
            {
                lseries2Point3Ds.Add(new SeriesPoint3D(x, y, z));
                lseries2Point3Ds.Add(new SeriesPoint3D(x, 0, z));
                lseries2Point3Ds.Add(new SeriesPoint3D(x, y, z));
            }
            else
            {
                lseries2Point3Ds.RemoveAt(0);
                lseries2Point3Ds.RemoveAt(0);
                lseries2Point3Ds.RemoveAt(0);
                lseries2Point3Ds.Add(new SeriesPoint3D(x, y, z));
                lseries2Point3Ds.Add(new SeriesPoint3D(x, 0, z));
                lseries2Point3Ds.Add(new SeriesPoint3D(x, y, z));
            }

            dataSeries.Points = lseriesPoint3Ds.ToArray();

            dataSeries2.Points = lseries2Point3Ds.ToArray();

        }

        double point3 = 1.0;
        int count3 = 0;
        private void dispatcherTimer_Tick3(object sender, EventArgs e)
        {
            point3 += 2.0;

            if (point3 >= 360) point3 = point3 - 360;

            if (count3 == aeroScopes.Count)
            {
                count3 = 0;
            }

            float x = aeroScopes[count3].latitude;
            float z = aeroScopes[count3].longitude;

            var ywidth = (float)(_chart.View3D.YAxisPrimary3D.Maximum - _chart.View3D.YAxisPrimary3D.Minimum);
            double y = Math.Cos(2 * Math.PI * point3 / 360) * (ywidth / 4) + (ywidth / 2);


            DroneModel.Position.X = (float)x;
            DroneModel.Position.Y = (float)y;
            DroneModel.Position.Z = (float)z;

            _chart.View3D.Annotations[0].TargetAxisValues.SetValues(DroneModel.Position.X, DroneModel.Position.Y, DroneModel.Position.Z);
            _chart.View3D.Annotations[0].Text = "X: " + DroneModel.Position.X.ToString("F6") + "\nY: " + DroneModel.Position.Y.ToString("F0") + "\nZ: " + DroneModel.Position.Z.ToString("F6");


            if (lseriesPoint3Ds.Count < 1000)
            {
                lseriesPoint3Ds.Add(new SeriesPoint3D(x, y, z));
            }
            else
            {
                lseriesPoint3Ds.RemoveAt(0);
                lseriesPoint3Ds.Add(new SeriesPoint3D(x, y, z));
            }

            if (lseries2Point3Ds.Count < 3000)
            {
                lseries2Point3Ds.Add(new SeriesPoint3D(x, y, z));
                lseries2Point3Ds.Add(new SeriesPoint3D(x, 0, z));
                lseries2Point3Ds.Add(new SeriesPoint3D(x, y, z));
            }
            else
            {
                lseries2Point3Ds.RemoveAt(0);
                lseries2Point3Ds.RemoveAt(0);
                lseries2Point3Ds.RemoveAt(0);
                lseries2Point3Ds.Add(new SeriesPoint3D(x, y, z));
                lseries2Point3Ds.Add(new SeriesPoint3D(x, 0, z));
                lseries2Point3Ds.Add(new SeriesPoint3D(x, y, z));
            }

            dataSeries.Points = lseriesPoint3Ds.ToArray();

            dataSeries2.Points = lseries2Point3Ds.ToArray();

            count3++;
        }


        List<SeriesPoint3D> lseriesPoint3Ds = new List<SeriesPoint3D>();

        List<SeriesPoint3D> lseries2Point3Ds = new List<SeriesPoint3D>();

        List<SeriesPoint3D> lseriesPoint3DsRDM = new List<SeriesPoint3D>();

        List<SeriesPoint3D> lseries2Point3DsRDM = new List<SeriesPoint3D>();

        private void Button3_Click(object sender, RoutedEventArgs e)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();

            string strResourceStream = assembly.GetName().Name + "." + "Resources.F.obj";

            MeshModel tempModel = new MeshModel(_chart.View3D, Axis3DBinding.Primary, Axis3DBinding.Primary, Axis3DBinding.Primary);
            tempModel.LoadFromResource(strResourceStream);
            //tempModel.Position.X = 1500;
            //tempModel.Position.Y = 0;
            //tempModel.Position.Z = 1500;

            tempModel.Position.X = (float)_chart.View3D.XAxisPrimary3D.Minimum + (float)(_chart.View3D.XAxisPrimary3D.Maximum - _chart.View3D.XAxisPrimary3D.Minimum) * 0.5f;
            tempModel.Position.Y = 0;
            tempModel.Position.Z = (float)_chart.View3D.ZAxisPrimary3D.Minimum + (float)(_chart.View3D.ZAxisPrimary3D.Maximum - _chart.View3D.ZAxisPrimary3D.Minimum) * 0.5f;


            //tempModel.MouseInteraction = true;
            tempModel.MouseInteraction = false;
            tempModel.Rotation.SetValues(0, 0, 0);
            //tempModel.Size.SetValues(5.0f, 5.0f, 5.0f);
            tempModel.Size.SetValues(2.5f, 2.5f, 2.5f);
            _chart.View3D.MeshModels.Add(tempModel);
        }

        private void Button4_Click(object sender, RoutedEventArgs e)
        {
            string path = "C:\\Users\\User\\Documents\\visual studio 2017\\Projects\\try_3D\\try_3D\\Resources\\Budynek 5.obj";

            Assembly assembly = Assembly.GetExecutingAssembly();
            string strResourceStream = assembly.GetName().Name + "." + "Resources.Budynek 5.obj";

            PalaceModel = new MeshModel(_chart.View3D, Axis3DBinding.Primary, Axis3DBinding.Primary, Axis3DBinding.Primary);
            //PalaceModel.LoadFromFile(path);

            PalaceModel.LoadFromResource(strResourceStream);

            PalaceModel.Position.X = 1500;
            PalaceModel.Position.Y = 0;
            PalaceModel.Position.Z = 1500;
            PalaceModel.MouseInteraction = true;
            PalaceModel.Rotation.SetValues(0, 0, 0);
            PalaceModel.Size.SetValues(30.0f, 30.0f, 30.0f);
            _chart.View3D.MeshModels.Add(PalaceModel);
        }

        private void Button5_Click(object sender, RoutedEventArgs e)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            string strResourceStream = assembly.GetName().Name + "." + "Resources.low-poly-mill.obj";

            MeshModel Mill = new MeshModel(_chart.View3D, Axis3DBinding.Primary, Axis3DBinding.Primary, Axis3DBinding.Primary);

            Mill.LoadFromResource(strResourceStream);

            Mill.Position.X = 1500;
            Mill.Position.Y = 0;
            Mill.Position.Z = 1500;
            Mill.MouseInteraction = true;
            Mill.Rotation.SetValues(0, 0, 0);
            Mill.Size.SetValues(0.5f, 0.5f, 0.5f);
            _chart.View3D.MeshModels.Add(Mill);
        }

        private void Button6_Click(object sender, RoutedEventArgs e)
        {
            Bitmap bitmap = (Bitmap)ChartTools.ImageFromResource("Resources.SeaBottom.jpg", Assembly.GetExecutingAssembly());
            CreateSeaBottomSurface(bitmap);
        }

        private void Button7_Click(object sender, RoutedEventArgs e)
        {
            Bitmap bitmap = new Bitmap(300, 300);

            Random random = new Random();
            for (int x = 0; x < bitmap.Width; x++)
                for (int y = 0; y < bitmap.Height; y++)
                {
                    if (y == 0)
                        bitmap.SetPixel(x, y, System.Drawing.Color.FromArgb(255, 255, 255, 255));
                    else
                    {
                        var d = random.NextDouble();
                        var r = bitmap.GetPixel(x, y - 1).R;
                        if (d <= 0.5)
                        {
                            if (r == 0) bitmap.SetPixel(x, y, System.Drawing.Color.FromArgb(255, r, r, r));
                            else bitmap.SetPixel(x, y, System.Drawing.Color.FromArgb(255, r - 1, r - 1, r - 1));
                        }
                        else
                        {
                            if (r == 255) bitmap.SetPixel(x, y, System.Drawing.Color.FromArgb(255, r, r, r));
                            else bitmap.SetPixel(x, y, System.Drawing.Color.FromArgb(255, r + 1, r + 1, r + 1));
                        }
                    }
                    //var v = new Random().Next(0, 255);
                    //bitmap.SetPixel(x, y, System.Drawing.Color.FromArgb(255, new Random().Next(0, 255), new Random().Next(0, 255), new Random().Next(0, 255)));

                    //bitmap.SetPixel(x, y, System.Drawing.Color.FromArgb(255, random.Next(250,255), random.Next(250, 255), random.Next(250, 255)));

                    //if (x>100 && x<200)
                    //bitmap.SetPixel(x, y, System.Drawing.Color.FromArgb(255, random.Next(0, x), random.Next(0, x), random.Next(0, x)));
                }
            CreateSeaBottomSurface2(bitmap);
        }

        private void buttonOpenObjFile_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog();
            //dialog.InitialDirectory = @"E:\Downloads\3D";
            dialog.Filter = "*.OBJ  | *.obj";
            if (dialog.ShowDialog() == true)
            {
                //Disable rendering, strongly recommended before updating chart properties
                _chart.BeginUpdate();

                MeshModel model = new MeshModel(_chart.View3D, Axis3DBinding.Primary, Axis3DBinding.Primary, Axis3DBinding.Primary);
                model.ModelFile = dialog.FileName;

                model.Position.X = (float)_chart.View3D.XAxisPrimary3D.Minimum + (float)(_chart.View3D.XAxisPrimary3D.Maximum - _chart.View3D.XAxisPrimary3D.Minimum) * 0.5f;

                //model.Position.Y = (float)_chart.View3D.YAxisPrimary3D.Minimum + (float)(_chart.View3D.YAxisPrimary3D.Maximum - _chart.View3D.YAxisPrimary3D.Minimum) * 0.5f;
                model.Position.Y = 0;

                model.Position.Z = (float)_chart.View3D.ZAxisPrimary3D.Minimum + (float)(_chart.View3D.ZAxisPrimary3D.Maximum - _chart.View3D.ZAxisPrimary3D.Minimum) * 0.5f;

                //model.Position.X = 100;
                //model.Position.Y = 100;
                //model.Position.Z = 100;

                model.Rotation.SetValues(0, 0, 0);

                float d = 0;
                try
                {
                    if (TBSize.Text.Contains("."))
                        TBSize.Text = TBSize.Text.Replace(".", ",");
                    d = (float)Convert.ToDouble(TBSize.Text);
                }
                catch { d = 0; }
                if (d == 0)
                    model.Size.SetValues(1f, 1f, 1f);
                else
                    model.Size.SetValues(1f * d, 1f * d, 1f * d);

                model.MouseInteraction = false;
                _chart.View3D.MeshModels.Add(model);

                //Allow chart rendering
                _chart.EndUpdate();
            }
        }


        private void buttonOpenTIFFile_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog();
            //dialog.InitialDirectory = @"E:\Downloads\3D";
            dialog.Filter = "*.TIF  | *.tif";
            if (dialog.ShowDialog() == true)
            {
                //Disable rendering, strongly recommended before updating chart properties
                _chart.BeginUpdate();

                Bitmap bitmap = new Bitmap(dialog.FileName);
                CreateSeaBottomSurface2(bitmap);

                //Allow chart rendering
                _chart.EndUpdate();
            }
        }

        private void buttonOpenJPGFile_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog();
            //dialog.InitialDirectory = @"E:\Downloads\3D";
            dialog.Filter = "*.JPG  | *.jpg";
            if (dialog.ShowDialog() == true)
            {
                //Disable rendering, strongly recommended before updating chart properties
                _chart.BeginUpdate();

                Bitmap bitmap = new Bitmap(dialog.FileName);
                CreateSeaBottomSurface(bitmap);

                //Allow chart rendering
                _chart.EndUpdate();
            }
        }

        private void buttonNewOBJLoad_Click(object sender, RoutedEventArgs e)
        {
            // Add loaded CAD model to the scene3D.
            MeshModel meshModel = new MeshModel();
            meshModel.LoadFromFile(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) + "\\Content\\A340-600_OBJ.obj");
            float sizeFactor = 0.4f;
            meshModel.Size.SetValues(sizeFactor, sizeFactor, sizeFactor);
            meshModel.Position.SetValues(0, -15, 0);
            meshModel.Rotation.SetValues(0, 0, 0);
            var DefaultColor = System.Windows.Media.Color.FromArgb(255, 100, 100, 100);
            meshModel.WireframeLineColor = DefaultColor;
        }

        private void buttonOriginalView_Click(object sender, RoutedEventArgs e)
        {
            var m_view = _chart.View3D;
            m_view.Camera.RotationX = 20;
            m_view.Camera.RotationY = -25;
            m_view.Camera.RotationZ = 0;
            m_view.Camera.ViewDistance = 466;

            m_view.Camera.OrientationMode = OrientationModes.XYZ_Mixed;
            m_view.Camera.Projection = ProjectionType.Perspective;
        }

        private void Button8_Click(object sender, RoutedEventArgs e)
        {
            Sleep5();
            Console.WriteLine("Button8");
        }

        private async Task Sleep5()
        {
            await Task.Delay(5000);
            MessageBox.Show("Sleep 5 sec");
        }

        private void Button9_Click(object sender, RoutedEventArgs e)
        {
            //Task.Run(() => Sleep5());// работает
            //await Task.Run(() => Sleep5());
            //await Task.Factory.StartNew(() => Sleep5(), TaskCreationOptions.LongRunning);
            //Task.Factory.StartNew(() => Sleep5(), TaskCreationOptions.LongRunning);
            //Dispatcher.Invoke(() => Sleep5());  //не подходит
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, new Action(() => Sleep5()));
            Console.WriteLine("Button9");
        }

        private void Button10_Click(object sender, RoutedEventArgs e)
        {
            _chart.View3D.XAxisPrimary3D.Visible = !_chart.View3D.XAxisPrimary3D.Visible;
            _chart.View3D.XAxisPrimary3D.LabelsVisible = !_chart.View3D.XAxisPrimary3D.LabelsVisible;
            _chart.View3D.YAxisPrimary3D.Visible = !_chart.View3D.YAxisPrimary3D.Visible;
            _chart.View3D.YAxisPrimary3D.LabelsVisible = !_chart.View3D.YAxisPrimary3D.LabelsVisible;
            _chart.View3D.ZAxisPrimary3D.Visible = !_chart.View3D.ZAxisPrimary3D.Visible;
            _chart.View3D.ZAxisPrimary3D.LabelsVisible = !_chart.View3D.ZAxisPrimary3D.LabelsVisible;
        }

        private void Button11_Click(object sender, RoutedEventArgs e)
        {
            _chart.View3D.MeshModels.Clear();
        }

        private void Button12_Click(object sender, RoutedEventArgs e)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();

            string strResourceStream = assembly.GetName().Name + "." + "Resources.MQ-27.obj";

            DroneModel = new MeshModel(
                _chart.View3D,
                Axis3DBinding.Primary,
                Axis3DBinding.Primary,
                Axis3DBinding.Primary);
            DroneModel.LoadFromResource(strResourceStream);
            DroneModel.Position.X = 150;
            DroneModel.Position.Y = 150;
            DroneModel.Position.Z = 150;
            DroneModel.MouseInteraction = true;
            DroneModel.Rotation.SetValues(0, 0, 0);
            DroneModel.Size.SetValues(0.02f, 0.020f, 0.020f);
            _chart.View3D.MeshModels.Add(DroneModel);
        }

        private void Button13_Click(object sender, RoutedEventArgs e)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();

            string strResourceStream = assembly.GetName().Name + "." + "Resources.MQ-27-2.obj";

            DroneModel = new MeshModel(
                _chart.View3D,
                Axis3DBinding.Primary,
                Axis3DBinding.Primary,
                Axis3DBinding.Primary);
            DroneModel.LoadFromResource(strResourceStream);
            //DroneModel.Position.X = 150;
            //DroneModel.Position.Y = 150;
            //DroneModel.Position.Z = 150;

            DroneModel.Position.X = (float)_chart.View3D.XAxisPrimary3D.Minimum + (float)(_chart.View3D.XAxisPrimary3D.Maximum - _chart.View3D.XAxisPrimary3D.Minimum) / 20.0f;
            //DroneModel.Position.Y = (float)_chart.View3D.YAxisPrimary3D.Minimum + (float)(_chart.View3D.YAxisPrimary3D.Maximum - _chart.View3D.YAxisPrimary3D.Minimum) / 20.0f;
            DroneModel.Position.Y = 150;
            DroneModel.Position.Z = (float)_chart.View3D.ZAxisPrimary3D.Minimum + (float)(_chart.View3D.ZAxisPrimary3D.Maximum - _chart.View3D.ZAxisPrimary3D.Minimum) / 20.0f;

            DroneModel.MouseInteraction = true;

            DroneModel.MouseClick += DroneModel_MouseClick;

            DroneModel.Rotation.SetValues(0, 0, 0);
            DroneModel.Size.SetValues(0.02f, 0.02f, 0.02f);
            _chart.View3D.MeshModels.Add(DroneModel);

            AddAnnotation();

            _chart.View3D.Annotations[0].TargetAxisValues.SetValues(DroneModel.Position.X, DroneModel.Position.Y, DroneModel.Position.Z);
            //_chart.View3D.Annotations[0].Text = "X: " + DroneModel.Position.X.ToString("F0") + "\nY: " + DroneModel.Position.Y.ToString("F0") + "\nZ: " + DroneModel.Position.Z.ToString("F0");
            _chart.View3D.Annotations[0].Text = "X: " + DroneModel.Position.X.ToString("F6") + "\nY: " + DroneModel.Position.Y.ToString("F0") + "\nZ: " + DroneModel.Position.Z.ToString("F6");

        }

        private void DroneModel_MouseClick(object sender, MouseEventArgs e)
        {
            _chart.View3D.Annotations[0].Visible = !_chart.View3D.Annotations[0].Visible;
        }

        private void AddAnnotation()
        {
            Annotation3D targetValueLabel = new Annotation3D(_chart.View3D, Axis3DBinding.Primary, Axis3DBinding.Primary, Axis3DBinding.Primary);

            targetValueLabel.TargetCoordinateSystem = AnnotationTargetCoordinates.AxisValues;

            targetValueLabel.LocationCoordinateSystem = CoordinateSystem.RelativeCoordinatesToTarget;
            //targetValueLabel.LocationCoordinateSystem = CoordinateSystem.AxisValues;

            //targetValueLabel.LocationRelativeOffset.SetValues(40, -70);

            //targetValueLabel.LocationAxisValues = new PointDoubleXYZ(_chart.View3D, 1000, 2000, 500);

            targetValueLabel.Visible = false;
            targetValueLabel.MouseInteraction = false;
            targetValueLabel.Style = AnnotationStyle.RoundedCallout;
            targetValueLabel.Shadow.Visible = false;
            _chart.View3D.Annotations.Add(targetValueLabel);
        }

        private void Button14_Click(object sender, RoutedEventArgs e)
        {
            //_chart.View3D.Annotations[0].Visible = !_chart.View3D.Annotations[0].Visible;
            for (int i = 0; i < _chart.View3D.Annotations.Count(); i++)
            {
                _chart.View3D.Annotations[i].Visible = !_chart.View3D.Annotations[i].Visible;
            }
        }

        private void Button16_Click(object sender, RoutedEventArgs e)
        {
            Bitmap bitmap = new Bitmap("1.tif");
            CreateSeaBottomSurface2(bitmap);
        }

        private void Button17_Click(object sender, RoutedEventArgs e)
        {
            Bitmap bitmap = new Bitmap(2, 2);

            //bitmap.SetPixel(0, 0, System.Drawing.Color.FromArgb(255, 255, 0, 0));
            //bitmap.SetPixel(0, 1, System.Drawing.Color.FromArgb(255, 0, 255, 0));
            //bitmap.SetPixel(0, 2, System.Drawing.Color.FromArgb(255, 0, 0, 255));
            //bitmap.SetPixel(1, 0, System.Drawing.Color.FromArgb(255, 255, 255, 255));
            //bitmap.SetPixel(1, 1, System.Drawing.Color.FromArgb(255, 128, 128, 128));
            //bitmap.SetPixel(1, 2, System.Drawing.Color.FromArgb(255, 128, 0, 0));
            //bitmap.SetPixel(2, 0, System.Drawing.Color.FromArgb(255, 0, 128, 0));
            //bitmap.SetPixel(2, 1, System.Drawing.Color.FromArgb(255, 0, 0, 128));
            //bitmap.SetPixel(2, 2, System.Drawing.Color.FromArgb(255, 0, 0, 0));

            bitmap.SetPixel(0, 0, System.Drawing.Color.FromArgb(255, 128, 0, 0));
            bitmap.SetPixel(0, 1, System.Drawing.Color.FromArgb(255, 0, 128, 0));
            bitmap.SetPixel(1, 0, System.Drawing.Color.FromArgb(255, 0, 0, 128));
            bitmap.SetPixel(1, 1, System.Drawing.Color.FromArgb(255, 0, 0, 0));


            _chart.View3D.XAxisPrimary3D.SetRange(0, 2);

            //Y axis shows the depth
            _chart.View3D.YAxisPrimary3D.SetRange(0, 1000);

            //Z axis shows latitude
            _chart.View3D.ZAxisPrimary3D.SetRange(0, 2);

            CreateSeaBottomSurface(bitmap);
            //CreateSeaBottomSurface2(bitmap);
        }


        private void PalleteColors()
        {
            var color0 = System.Windows.Media.Color.FromArgb(255, 0, 0, 255);
            var color1 = System.Windows.Media.Color.FromArgb(255, 51, 153, 255);
            var color2 = System.Windows.Media.Color.FromArgb(255, 128, 255, 255);
            var color3 = System.Windows.Media.Color.FromArgb(255, 36, 118, 30);
            var color4 = System.Windows.Media.Color.FromArgb(255, 48, 136, 24);
            var color5 = System.Windows.Media.Color.FromArgb(255, 60, 154, 18);
            var color6 = System.Windows.Media.Color.FromArgb(255, 72, 172, 12);
            var color7 = System.Windows.Media.Color.FromArgb(255, 84, 190, 6);
            var color8 = System.Windows.Media.Color.FromArgb(255, 160, 192, 0);
            var color9 = System.Windows.Media.Color.FromArgb(255, 172, 200, 0);
            var color10 = System.Windows.Media.Color.FromArgb(255, 184, 208, 0);
            var color11 = System.Windows.Media.Color.FromArgb(255, 196, 216, 0);
            var color12 = System.Windows.Media.Color.FromArgb(255, 208, 224, 0);
            var color13 = System.Windows.Media.Color.FromArgb(255, 220, 232, 0);
            var color14 = System.Windows.Media.Color.FromArgb(255, 232, 240, 0);
            var color15 = System.Windows.Media.Color.FromArgb(255, 244, 248, 0);
            var color16 = System.Windows.Media.Color.FromArgb(255, 240, 224, 0);
            var color17 = System.Windows.Media.Color.FromArgb(255, 234, 210, 0);
            var color18 = System.Windows.Media.Color.FromArgb(255, 228, 196, 0);
            var color19 = System.Windows.Media.Color.FromArgb(255, 222, 182, 0);
            var color20 = System.Windows.Media.Color.FromArgb(255, 216, 168, 0);
            var color21 = System.Windows.Media.Color.FromArgb(255, 210, 154, 0);
            var color22 = System.Windows.Media.Color.FromArgb(255, 204, 140, 0);
            var color23 = System.Windows.Media.Color.FromArgb(255, 198, 126, 0);
            var color24 = System.Windows.Media.Color.FromArgb(255, 192, 112, 0);
            var color25 = System.Windows.Media.Color.FromArgb(255, 186, 98, 0);
            var color26 = System.Windows.Media.Color.FromArgb(255, 180, 84, 0);
            var color27 = System.Windows.Media.Color.FromArgb(255, 174, 70, 0);
            var color28 = System.Windows.Media.Color.FromArgb(255, 168, 56, 0);
            var color29 = System.Windows.Media.Color.FromArgb(255, 255, 255, 255);
        }

        private void PalleteColors2()
        {
            var color0 = System.Windows.Media.Color.FromArgb(255, 0, 64, 48);
            var color1 = System.Windows.Media.Color.FromArgb(255, 12, 82, 42);
            var color2 = System.Windows.Media.Color.FromArgb(255, 24, 100, 36);
            var color3 = System.Windows.Media.Color.FromArgb(255, 36, 118, 30);
            var color4 = System.Windows.Media.Color.FromArgb(255, 48, 136, 24);
            var color5 = System.Windows.Media.Color.FromArgb(255, 60, 154, 18);
            var color6 = System.Windows.Media.Color.FromArgb(255, 72, 172, 12);
            var color7 = System.Windows.Media.Color.FromArgb(255, 84, 190, 6);
            var color8 = System.Windows.Media.Color.FromArgb(255, 160, 192, 0);
            var color9 = System.Windows.Media.Color.FromArgb(255, 172, 200, 0);
            var color10 = System.Windows.Media.Color.FromArgb(255, 184, 208, 0);
            var color11 = System.Windows.Media.Color.FromArgb(255, 196, 216, 0);
            var color12 = System.Windows.Media.Color.FromArgb(255, 208, 224, 0);
            var color13 = System.Windows.Media.Color.FromArgb(255, 220, 232, 0);
            var color14 = System.Windows.Media.Color.FromArgb(255, 232, 240, 0);
            var color15 = System.Windows.Media.Color.FromArgb(255, 244, 248, 0);
            var color16 = System.Windows.Media.Color.FromArgb(255, 240, 224, 0);
            var color17 = System.Windows.Media.Color.FromArgb(255, 234, 210, 0);
            var color18 = System.Windows.Media.Color.FromArgb(255, 228, 196, 0);
            var color19 = System.Windows.Media.Color.FromArgb(255, 222, 182, 0);
            var color20 = System.Windows.Media.Color.FromArgb(255, 216, 168, 0);
            var color21 = System.Windows.Media.Color.FromArgb(255, 210, 154, 0);
            var color22 = System.Windows.Media.Color.FromArgb(255, 204, 140, 0);
            var color23 = System.Windows.Media.Color.FromArgb(255, 198, 126, 0);
            var color24 = System.Windows.Media.Color.FromArgb(255, 192, 112, 0);
            var color25 = System.Windows.Media.Color.FromArgb(255, 186, 98, 0);
            var color26 = System.Windows.Media.Color.FromArgb(255, 180, 84, 0);
            var color27 = System.Windows.Media.Color.FromArgb(255, 174, 70, 0);
            var color28 = System.Windows.Media.Color.FromArgb(255, 168, 56, 0);
            var color29 = System.Windows.Media.Color.FromArgb(255, 162, 42, 0);
            var color30 = System.Windows.Media.Color.FromArgb(255, 156, 28, 0);
            var color31 = System.Windows.Media.Color.FromArgb(255, 150, 14, 0);
        }


        System.Windows.Media.Color[] m_colors = new System.Windows.Media.Color[10];
        void MakeSecretFormula(int nA)
        {
            int count = m_colors.Length / 4;
            fillColorsHelper(0, count, nA, 0, 64, 48, 96, 208, 0);
            fillColorsHelper(count, count, nA, 160, 192, 0, 255, 255, 0);
            fillColorsHelper(count * 2, m_colors.Length - count * 2, nA, 240, 224, 0, 144, 0, 0);
        }

        void fillColorsHelper(int nStartIdx, int nCount, int a, int startR, int startG, int startB,
        int endR, int endG, int endB)
        {
            int deltaRed = (endR - startR) / nCount;
            int deltaGreen = (endG - startG) / nCount;
            int deltaBlue = (endB - startB) / nCount;
            for (int i = 0; i < nCount; i++)
                m_colors[nStartIdx + i] = System.Windows.Media.Color.FromArgb((byte)a, (byte)(startR + i * deltaRed), (byte)(startG + i * deltaGreen), (byte)(startB + i * deltaBlue));
        }


        private double calculateTheDistance(double latA, double lonA, double latB, double lonB)
        {
            double EARTH_RADIUS = 6372795;

            /*
            * Расстояние между двумя точками
            * $φA, $λA - широта, долгота 1-й точки,
            * $φB, $λB - широта, долгота 2-й точки
            *
            */

            // перевести координаты в радианы
            var lat1 = latA * Math.PI / 180;
            var lat2 = latB * Math.PI / 180;
            var long1 = lonA * Math.PI / 180;
            var long2 = lonB * Math.PI / 180;

            // косинусы и синусы широт и разницы долгот
            var cl1 = Math.Cos(lat1);
            var cl2 = Math.Cos(lat2);
            var sl1 = Math.Sin(lat1);
            var sl2 = Math.Sin(lat2);
            var delta = long2 - long1;
            var cdelta = Math.Cos(delta);
            var sdelta = Math.Sin(delta);

            // вычисления длины большого круга
            var y = Math.Sqrt(Math.Pow(cl2 * sdelta, 2) + Math.Pow(cl1 * sl2 - sl1 * cl2 * cdelta, 2));
            var x = sl1 * sl2 + cl1 * cl2 * cdelta;

            //
            var ad = Math.Atan2(y, x);
            var dist = ad * EARTH_RADIUS;

            return dist;
        }

        private void Button18_Click(object sender, RoutedEventArgs e)
        {
            List<Rectangle3D> rectangle3Ds = new List<Rectangle3D>();

            _chart.View3D.Rectangles.Clear();

            //RectanglesCircle(System.Windows.Media.Color.FromArgb(255, 255, 0, 0), 3.0d);

           //RectanglesCircle(System.Windows.Media.Color.FromArgb(255, 255, 255, 0), 2.0d);

           //RectanglesCircle(System.Windows.Media.Color.FromArgb(255, 0, 255, 0), 1.5d);

           (double Latitude, double Longitude) CenterPoint = (53.931672, 27.636670);

            (double Latitude, double Longitude) CenterPoint2 = (53.931272, 27.636970);

            double standartX = 484;
            double standartZ = 478;

            double coef3 = 3.0d;

            var color1 = (System.Windows.Media.Color.FromArgb(Convert.ToByte(a1.Text), Convert.ToByte(r1.Text), Convert.ToByte(g1.Text), Convert.ToByte(b1.Text)));

            if (increment.Text.Contains(".")) increment.Text = increment.Text.Replace(".", ",");
            double incr = Convert.ToDouble(increment.Text);
            if (wwidth.Text.Contains(".")) wwidth.Text = wwidth.Text.Replace(".", ",");
            double width = Convert.ToDouble(wwidth.Text);
            int material = Convert.ToInt32(cbMaterial.SelectedIndex);

            RectanglesCircle2(color1, CenterPoint2, standartX / coef3, standartZ / coef3, inc: incr, recWidth: width, Material: material);

            double coef2 = 2.0d;

            var color2 = (System.Windows.Media.Color.FromArgb(Convert.ToByte(a2.Text), Convert.ToByte(r2.Text), Convert.ToByte(g2.Text), Convert.ToByte(b2.Text)));

            RectanglesCircle2(color2, CenterPoint, standartX / coef2, standartZ / coef2, inc: incr, recWidth: width, Material: material);

            _chart.View3D.Rectangles.AddRange(rectangle3Ds);


            void RectanglesCircle(System.Windows.Media.Color color, double coef = 3.0d, double inc = 0.5d, double recHeight = 10.0d)
            {
                var xstart = _chart.View3D.XAxisPrimary3D.Minimum;
                var zstart = _chart.View3D.ZAxisPrimary3D.Minimum;
                //var ystart = _chart.View3D.YAxisPrimary3D.Minimum;
                //var ystart =0;

                var xwidth = (float)(_chart.View3D.XAxisPrimary3D.Maximum - _chart.View3D.XAxisPrimary3D.Minimum);
                var zwidth = (float)(_chart.View3D.ZAxisPrimary3D.Maximum - _chart.View3D.ZAxisPrimary3D.Minimum);
                //var ywidth = (float)(_chart.View3D.YAxisPrimary3D.Maximum - _chart.View3D.YAxisPrimary3D.Minimum);

               
                for (double point = 0; point < 360d; point += inc)
                {
                    // Rotated small rectangle.
                    Rectangle3D r3 = new Rectangle3D(_chart.View3D, Axis3DBinding.Primary, Axis3DBinding.Primary, Axis3DBinding.Primary);

                    r3.Size.Width = 1;
                    r3.Size.Height = recHeight;

                    double x = Math.Cos(2 * Math.PI * point / 360) * (xwidth / coef) + xstart + (xwidth / 2);
                    double z = Math.Sin(2 * Math.PI * point / 360) * (zwidth / coef) + zstart + (zwidth / 2);
                    //double y = Math.Cos(2 * Math.PI * point / 360) * (ywidth / 4) + (ywidth / 2);

                    r3.Center.SetValues(x, r3.Size.Height * 2, z);

                    r3.Fill.UseImage = false;
                    //r3.Fill.Material.DiffuseColor = System.Windows.Media.Color.FromArgb(150, 0, 0, 255);
                    //r3.Fill.Material.DiffuseColor = System.Windows.Media.Color.FromArgb(255, 0, 0, 255);

                    //r3.Fill.Material.AmbientColor = System.Windows.Media.Color.FromArgb(255, 255, 0, 0);

                    r3.Fill.Material.AmbientColor = color;

                    //r3.Fill.Material.EmissiveColor = System.Windows.Media.Color.FromArgb(255, 0, 0, 255);

                    //r3.Fill.Material.SpecularColor = System.Windows.Media.Color.FromArgb(255, 0, 0, 255);


                    r3.Rotation.SetValues(90, 0, point);

                    r3.MouseHighlight = MouseOverHighlight.None;

                    rectangle3Ds.Add(r3);

                }
            }

            void RectanglesCircle2(System.Windows.Media.Color color, (double Latitude, double Longitude) CenterCoord, double RadiusX, double RadiusZ, double inc = 1.0d, double recWidth = 1.0, double recHeight = 30.0d, int Material = 1)
            {
                var xstart = _chart.View3D.XAxisPrimary3D.Minimum;
                var zstart = _chart.View3D.ZAxisPrimary3D.Minimum;
                //var ystart = _chart.View3D.YAxisPrimary3D.Minimum;
                //var ystart =0;

                var xwidth = (float)(_chart.View3D.XAxisPrimary3D.Maximum - _chart.View3D.XAxisPrimary3D.Minimum);
                var zwidth = (float)(_chart.View3D.ZAxisPrimary3D.Maximum - _chart.View3D.ZAxisPrimary3D.Minimum);
                //var ywidth = (float)(_chart.View3D.YAxisPrimary3D.Maximum - _chart.View3D.YAxisPrimary3D.Minimum);

                var d1 = calculateTheDistance(m_mapCoordCorner.Latitude, m_mapCoordCorner.Longitude, m_mapCoordMaxLat.Latitude, m_mapCoordMaxLat.Longitude);
                var d2 = calculateTheDistance(m_mapCoordCorner.Latitude, m_mapCoordCorner.Longitude, m_mapCoordMaxLon.Latitude, m_mapCoordMaxLon.Longitude);

                double coefX = (RadiusX * xwidth) / d1;
                double coefZ = (RadiusZ * zwidth) / d2;

                for (double point = 0; point < 360d; point += inc)
                {
                    // Rotated small rectangle.
                    Rectangle3D r3 = new Rectangle3D(_chart.View3D, Axis3DBinding.Primary, Axis3DBinding.Primary, Axis3DBinding.Primary);

                    r3.Size.Width = 1.0;
                    r3.Size.Height = recHeight;

                    double x = Math.Cos(2 * Math.PI * point / 360) * coefX + CenterCoord.Latitude;
                    double z = Math.Sin(2 * Math.PI * point / 360) * coefZ + CenterCoord.Longitude;
                    //double x = Math.Cos(2 * Math.PI * point / 360) * (xwidth / coef) + CenterCoord.Latitude;
                    //double z = Math.Sin(2 * Math.PI * point / 360) * (zwidth / coef) + CenterCoord.Longitude;
                    //double y = Math.Cos(2 * Math.PI * point / 360) * (ywidth / 4) + (ywidth / 2);

                    r3.Center.SetValues(x, r3.Size.Height * 2, z);

                    r3.Fill.UseImage = false;

                    switch (Material)
                    {
                        case 0:
                            r3.Fill.Material.DiffuseColor = color;
                            break;
                        case 1:
                            r3.Fill.Material.AmbientColor = color;
                            break;
                        case 2:
                            r3.Fill.Material.EmissiveColor = color;
                            break;

                        case 3:
                            r3.Fill.Material.SpecularColor = color;
                            break;
                    }

                    r3.Rotation.SetValues(90, 0, point);

                    r3.MouseHighlight = MouseOverHighlight.None;

                    r3.MouseInteraction = false;

                    rectangle3Ds.Add(r3);

                }
            }

            {
            //    //LoadFromFile
            //    MeshModel meshModel = new MeshModel();
            //    string temp = System.Reflection.Assembly.GetEntryAssembly().Location;
            //    var lastindex = temp.LastIndexOf("\\");
            //    temp = temp.Substring(0, lastindex);
            //    lastindex = temp.LastIndexOf("\\");
            //    temp = temp.Substring(0, lastindex);
            //    lastindex = temp.LastIndexOf("\\");
            //    temp = temp.Substring(0, lastindex);
            //    temp = temp + "\\Resources\\KB_RADAR_3d_landscape\\3d_landscape.obj";
            //    meshModel.LoadFromFile(temp);
            //    float sizeFactor = 0.4f;
            //    meshModel.Size.SetValues(sizeFactor, sizeFactor, sizeFactor);

            //    meshModel.Position.X = (float)_chart.View3D.XAxisPrimary3D.Minimum + (float)(_chart.View3D.XAxisPrimary3D.Maximum - _chart.View3D.XAxisPrimary3D.Minimum) / 20.0f;
            //    meshModel.Position.Y = 150;
            //    meshModel.Position.Z = (float)_chart.View3D.ZAxisPrimary3D.Minimum + (float)(_chart.View3D.ZAxisPrimary3D.Maximum - _chart.View3D.ZAxisPrimary3D.Minimum) / 20.0f;

            //    meshModel.Rotation.SetValues(0, 0, 0);
            //    var DefaultColor = System.Windows.Media.Color.FromArgb(255, 100, 100, 100);
            //    meshModel.WireframeLineColor = DefaultColor;

            //    _chart.View3D.MeshModels.Add(meshModel);
            }
        }



        private void Button19_Click(object sender, RoutedEventArgs e)
        {
            _chart.View3D.Rectangles[0].Rotation.Z += 45;

            {
                ////LoadFromStream

                //string temp = System.Reflection.Assembly.GetEntryAssembly().Location;
                //var lastindex = temp.LastIndexOf("\\");
                //temp = temp.Substring(0, lastindex);
                //lastindex = temp.LastIndexOf("\\");
                //temp = temp.Substring(0, lastindex);
                //lastindex = temp.LastIndexOf("\\");
                //temp = temp.Substring(0, lastindex);
                ////temp = temp + "\\Resources\\KB_RADAR_3d_landscape\\3d_landscape.obj";
                //temp = temp + "\\Resources\\KB_RADAR_3d_landscape";
                ////temp = temp + "\\Resources\\KB_RADAR_3d_landscape\\";


                //MeshModel meshModel = new MeshModel(_chart.View3D, Axis3DBinding.Primary, Axis3DBinding.Primary, Axis3DBinding.Primary);
                //MeshModel meshModel1 = new MeshModel(_chart.View3D, Axis3DBinding.Primary, Axis3DBinding.Primary, Axis3DBinding.Primary);


                //string StartDirectory = temp;
                //string EndDirectory = temp;

                //using (FileStream SourceStream = File.Open("3d_landscape.obj", FileMode.Open))
                //{
                //    using (FileStream DestinationStream = File.Create(EndDirectory + "\\3d_landscape.obj"))
                //    {
                //        meshModel1.LoadFromStream(DestinationStream);
                //    }
                //    meshModel.LoadFromStream(SourceStream);
                //}


                //meshModel.Position.X = (float)_chart.View3D.XAxisPrimary3D.Minimum + (float)(_chart.View3D.XAxisPrimary3D.Maximum - _chart.View3D.XAxisPrimary3D.Minimum) / 20.0f;
                //meshModel.Position.Y = 150;
                //meshModel.Position.Z = (float)_chart.View3D.ZAxisPrimary3D.Minimum + (float)(_chart.View3D.ZAxisPrimary3D.Maximum - _chart.View3D.ZAxisPrimary3D.Minimum) / 20.0f;

                //meshModel.MouseInteraction = true;
                //meshModel.Rotation.SetValues(0, 0, 0);
                //meshModel.Size.SetValues(0.5f, 0.5f, 0.5f);
                //_chart.View3D.MeshModels.Add(meshModel);
            }
        }

        private void Button20_Click(object sender, RoutedEventArgs e)
        {
            _chart.View3D.Rectangles[0].Rotation.Y += 45;
            {
                ////LoadFromResource

                //Assembly assembly = Assembly.GetExecutingAssembly();

                //string strResourceStream = assembly.GetName().Name + "." + "Resources.3d_landscape.obj";

                //MeshModel meshModel = new MeshModel(_chart.View3D, Axis3DBinding.Primary, Axis3DBinding.Primary, Axis3DBinding.Primary);
                //meshModel.LoadFromResource(strResourceStream);

                //meshModel.Position.X = (float)_chart.View3D.XAxisPrimary3D.Minimum + (float)(_chart.View3D.XAxisPrimary3D.Maximum - _chart.View3D.XAxisPrimary3D.Minimum) / 20.0f;
                //meshModel.Position.Y = 150;
                //meshModel.Position.Z = (float)_chart.View3D.ZAxisPrimary3D.Minimum + (float)(_chart.View3D.ZAxisPrimary3D.Maximum - _chart.View3D.ZAxisPrimary3D.Minimum) / 20.0f;

                //meshModel.MouseInteraction = true;
                //meshModel.Rotation.SetValues(0, 0, 0);
                //meshModel.Size.SetValues(0.5f, 0.5f, 0.5f);
                //_chart.View3D.MeshModels.Add(meshModel);
            }
        }

        private void MeshSize_Click(object sender, RoutedEventArgs e)
        {
            float f = 1;
            try
            {
                if (meshSizeBox.Text.Contains("."))
                    meshSizeBox.Text = meshSizeBox.Text.Replace(".", ",");
                f = (float)Convert.ToDouble(meshSizeBox.Text);
            }
            catch { f = 1; }

            _chart.View3D.MeshModels[0].Size.SetValues(f, f, f);
        }

        private void Xrotation_Click(object sender, RoutedEventArgs e)
        {
            float f = 0;
            try
            {
                if (xBox.Text.Contains("."))
                    xBox.Text = xBox.Text.Replace(".", ",");
                f = (float)Convert.ToDouble(xBox.Text);
            }
            catch { f = 0; }

            _chart.View3D.MeshModels[0].Rotation.SetValues(f, _chart.View3D.MeshModels[0].Rotation.Y, _chart.View3D.MeshModels[0].Rotation.Z);
        }

        private void Yrotation_Click(object sender, RoutedEventArgs e)
        {
            float f = 0;
            try
            {
                if (yBox.Text.Contains("."))
                    yBox.Text = yBox.Text.Replace(".", ",");
                f = (float)Convert.ToDouble(yBox.Text);
            }
            catch { f = 0; }

            _chart.View3D.MeshModels[0].Rotation.SetValues(_chart.View3D.MeshModels[0].Rotation.X, f, _chart.View3D.MeshModels[0].Rotation.Z);
        }

        private void Zrotation_Click(object sender, RoutedEventArgs e)
        {
            float f = 0;
            try
            {
                if (zBox.Text.Contains("."))
                    zBox.Text = zBox.Text.Replace(".", ",");
                f = (float)Convert.ToDouble(zBox.Text);
            }
            catch { f = 0; }

            _chart.View3D.MeshModels[0].Rotation.SetValues(_chart.View3D.MeshModels[0].Rotation.X, _chart.View3D.MeshModels[0].Rotation.Y, f);
        }

        private void MeshSizeBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                MeshSize_Click(this, null);
            }
        }

        private void XBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Xrotation_Click(this, null);
            }
        }

        private void YBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Yrotation_Click(this, null);
            }
        }

        private void ZBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Zrotation_Click(this, null);
            }
        }


        bool pinpoint = false;
        private void Pin_Click(object sender, RoutedEventArgs e)
        {
            double dlat = 0;
            double dlon = 0;
            double dalt = 0;
            try
            {
                if (lat.Text.Contains("."))
                    lat.Text = lat.Text.Replace(".", ",");
                dlat = Convert.ToDouble(lat.Text);

                if (lon.Text.Contains("."))
                    lon.Text = lon.Text.Replace(".", ",");
                dlon = Convert.ToDouble(lon.Text);

                if (alt.Text.Contains("."))
                    alt.Text = alt.Text.Replace(".", ",");
                dalt = Convert.ToDouble(alt.Text);
            }
            catch { }


            if (pinpoint == true)
            {
                //clear + add
                var pos = _chart.View3D.MeshModels.Count - 1;
                _chart.View3D.MeshModels.RemoveAt(pos);
            }

            Assembly assembly = Assembly.GetExecutingAssembly();

            string strResourceStream = assembly.GetName().Name + "." + "Resources.pin.obj";

            var PinModel = new MeshModel(
                _chart.View3D,
                Axis3DBinding.Primary,
                Axis3DBinding.Primary,
                Axis3DBinding.Primary);
            PinModel.LoadFromResource(strResourceStream);

            PinModel.Position.X = (float)dlat;
            PinModel.Position.Y = (float)dalt;
            PinModel.Position.Z = (float)dlon;

            PinModel.MouseInteraction = false;

            PinModel.Rotation.SetValues(90, 0, 0);
            PinModel.Size.SetValues(5f, 5f, 5f);
            _chart.View3D.MeshModels.Add(PinModel);

            pinpoint = true;
        }

        private void Button21_Click(object sender, RoutedEventArgs e)
        {
            DroneModel = new MeshModel(
                _chart.View3D,
                Axis3DBinding.Primary,
                Axis3DBinding.Primary,
                Axis3DBinding.Primary);

            string temp = System.Reflection.Assembly.GetEntryAssembly().Location;
            var lastindex = temp.LastIndexOf("\\");
            temp = temp.Substring(0, lastindex);
            lastindex = temp.LastIndexOf("\\");
            temp = temp.Substring(0, lastindex);
            lastindex = temp.LastIndexOf("\\");
            temp = temp.Substring(0, lastindex);
            temp = temp + "\\Resources\\UAV.obj";
            DroneModel.LoadFromFile(temp);

            DroneModel.Position.X = (float)_chart.View3D.XAxisPrimary3D.Minimum + (float)(_chart.View3D.XAxisPrimary3D.Maximum - _chart.View3D.XAxisPrimary3D.Minimum) / 20.0f;
            //DroneModel.Position.Y = (float)_chart.View3D.YAxisPrimary3D.Minimum + (float)(_chart.View3D.YAxisPrimary3D.Maximum - _chart.View3D.YAxisPrimary3D.Minimum) / 20.0f;
            DroneModel.Position.Y = 150;
            DroneModel.Position.Z = (float)_chart.View3D.ZAxisPrimary3D.Minimum + (float)(_chart.View3D.ZAxisPrimary3D.Maximum - _chart.View3D.ZAxisPrimary3D.Minimum) / 20.0f;

            DroneModel.MouseInteraction = true;

            DroneModel.MouseClick += DroneModel_MouseClick;

            DroneModel.Rotation.SetValues(0, 0, 0);
            DroneModel.Size.SetValues(0.02f, 0.02f, 0.02f);
            _chart.View3D.MeshModels.Add(DroneModel);

            AddAnnotation();

            _chart.View3D.Annotations[0].TargetAxisValues.SetValues(DroneModel.Position.X, DroneModel.Position.Y, DroneModel.Position.Z);
            _chart.View3D.Annotations[0].Text = "X: " + DroneModel.Position.X.ToString("F6") + "\nY: " + DroneModel.Position.Y.ToString("F0") + "\nZ: " + DroneModel.Position.Z.ToString("F6");

            AddUAV(ref DroneModelRDM);
        }

        private void AddUAV(ref MeshModel newDroneModel)
        {
            newDroneModel = new MeshModel(
                _chart.View3D,
                Axis3DBinding.Primary,
                Axis3DBinding.Primary,
                Axis3DBinding.Primary);

            string temp = System.Reflection.Assembly.GetEntryAssembly().Location;
            var lastindex = temp.LastIndexOf("\\");
            temp = temp.Substring(0, lastindex);
            lastindex = temp.LastIndexOf("\\");
            temp = temp.Substring(0, lastindex);
            lastindex = temp.LastIndexOf("\\");
            temp = temp.Substring(0, lastindex);
            temp = temp + "\\Resources\\UAV.obj";
            newDroneModel.LoadFromFile(temp);

            newDroneModel.Position.X = (float)_chart.View3D.XAxisPrimary3D.Minimum;
            newDroneModel.Position.Y = 150;
            newDroneModel.Position.Z = (float)_chart.View3D.ZAxisPrimary3D.Minimum;

            newDroneModel.MouseInteraction = true;

            //newDroneModel.MouseClick += DroneModel_MouseClick;

            newDroneModel.Rotation.SetValues(0, 0, 0);
            newDroneModel.Size.SetValues(0.02f, 0.02f, 0.02f);
            _chart.View3D.MeshModels.Add(newDroneModel);

            AddAnnotation();

            _chart.View3D.Annotations[_chart.View3D.Annotations.Count-1].TargetAxisValues.SetValues(newDroneModel.Position.X, newDroneModel.Position.Y, newDroneModel.Position.Z);
            _chart.View3D.Annotations[_chart.View3D.Annotations.Count - 1].Text = "X: " + newDroneModel.Position.X.ToString("F6") + "\nY: " + newDroneModel.Position.Y.ToString("F0") + "\nZ: " + newDroneModel.Position.Z.ToString("F6");
        }

        private void Button22_Click(object sender, RoutedEventArgs e)
        {
            LoadAeroScopeEmulator(trajBox.Text);
        }

        bool istraj = false;
        bool istrajRequest = false;

        bool istrajRDM = false;
        bool istrajRequestRDM = false;

        double point4 = 1;
        double point5 = 1;
        int count4 = 0;
        int count5 = 0;

        private void DispatchIfNecessary(Action action)
        {
            if (!Dispatcher.CheckAccess())
                Dispatcher.Invoke(action);
            else
                action.Invoke();
        }

        //Таск запроса
        private async void TrajRequest()
        {
            while (istrajRequest)
            {
                point4 += 2.0;

                if (point4 >= 360) point4 = point4 - 360;

                if (count4 == aeroScopes.Count)
                {
                    count4 = 0;
                    Button24_Click(this, null);
                }

                float x = aeroScopes[count4].latitude;
                float z = aeroScopes[count4].longitude;
                float y = (float)aeroScopes[count4].altitude;

                //var ywidth = (float)(_chart.View3D.YAxisPrimary3D.Maximum - _chart.View3D.YAxisPrimary3D.Minimum);
                DispatchIfNecessary(() =>
                {
                    if (altCheckBox.IsChecked.Value)
                    {
                        var ywidth = (float)(_chart.View3D.YAxisPrimary3D.Maximum - _chart.View3D.YAxisPrimary3D.Minimum);
                        y = (float)Math.Cos(2 * Math.PI * point4 / 360) * (ywidth / 4) + (ywidth / 2);
                    }

                    DroneModel.Position.X = (float)x;
                    DroneModel.Position.Y = (float)y;
                    DroneModel.Position.Z = (float)z;


                    (double Latitude, double Longitude) CenterPoint = (53.931672, 27.636670);
                    (double Latitude, double Longitude) CenterPoint2 = (53.931272, 27.636970);

                    var d = calculateTheDistance(CenterPoint.Latitude, CenterPoint.Longitude, x, z);
                    var d2 = calculateTheDistance(CenterPoint2.Latitude, CenterPoint2.Longitude, x, z);

                    double standartX = 484;
                    double standartZ = 478;

                    double coef2 = 2.0d;
                    double coef3 = 3.0d;


                    if (d < standartX / coef2 && d < standartZ / coef2)
                    {
                        _chart.View3D.Annotations[0].Fill.Color = System.Windows.Media.Color.FromArgb(255, 255, 255, 192);
                        _chart.View3D.Annotations[0].Fill.GradientColor = System.Windows.Media.Color.FromArgb(255, 255, 128, 0);
                        if (_chart.View3D.Annotations[0].Visible == false)
                            _chart.View3D.Annotations[0].Visible = true;

                        if (d2 < standartX / coef3 && d2 < standartZ / coef3)
                        {
                            _chart.View3D.Annotations[0].Fill.Color = System.Windows.Media.Color.FromArgb(255, 255, 0, 0);
                            _chart.View3D.Annotations[0].Fill.GradientColor = System.Windows.Media.Color.FromArgb(255, 255, 0, 0);
                        }
                    }
                    else
                    {
                        if (_chart.View3D.Annotations[0].Visible == true)
                            _chart.View3D.Annotations[0].Visible = false;
                    }

                    _chart.View3D.Annotations[0].TargetAxisValues.SetValues(DroneModel.Position.X, DroneModel.Position.Y + 10, DroneModel.Position.Z);
                    //_chart.View3D.Annotations[0].Text = "X: " + DroneModel.Position.X.ToString("F6") + "\nY: " + DroneModel.Position.Y.ToString("F0") + "\nZ: " + DroneModel.Position.Z.ToString("F6");
                    //_chart.View3D.Annotations[0].Text = "DJI Phantom4" + "\nF = 2412 MHz" + "\nH = " + DroneModel.Position.Y.ToString("F0") + " m";
                    _chart.View3D.Annotations[0].Text = settings3D.DroneModel + "\nF = " +settings3D.DroneFreq + "\nH = " + DroneModel.Position.Y.ToString("F0") + " m";



                    if (lseriesPoint3Ds.Count < tail1)
                    {
                        lseriesPoint3Ds.Add(new SeriesPoint3D(x, y, z));
                    }
                    else
                    {
                        lseriesPoint3Ds.RemoveAt(0);
                        lseriesPoint3Ds.Add(new SeriesPoint3D(x, y, z));
                    }

                    if (lseries2Point3Ds.Count < tail2)
                    {
                        lseries2Point3Ds.Add(new SeriesPoint3D(x, y, z));
                        lseries2Point3Ds.Add(new SeriesPoint3D(x, 0, z));
                        lseries2Point3Ds.Add(new SeriesPoint3D(x, y, z));
                    }
                    else
                    {
                        lseries2Point3Ds.RemoveAt(0);
                        lseries2Point3Ds.RemoveAt(0);
                        lseries2Point3Ds.RemoveAt(0);
                        lseries2Point3Ds.Add(new SeriesPoint3D(x, y, z));
                        lseries2Point3Ds.Add(new SeriesPoint3D(x, 0, z));
                        lseries2Point3Ds.Add(new SeriesPoint3D(x, y, z));
                    }

                    dataSeries.Points = lseriesPoint3Ds.ToArray();

                    dataSeries2.Points = lseries2Point3Ds.ToArray();

                });

                int Ti = count4;
                int Ti1 = count4 + 1;
                if (Ti1 == aeroScopes.Count) Ti1 = 0;

                int delay = Math.Abs(aeroScopes[Ti1].time - aeroScopes[Ti].time);
                if (delay > 10000) delay = 5000;
                //Console.WriteLine(delay);
                DispatchIfNecessary(() =>
                {
                    if (timeCheckBox.IsChecked.Value)
                    {
                        try
                        {
                            if (timeBox.Text.Contains("."))
                                timeBox.Text = timeBox.Text.Replace(".", ",");
                            double coef = Convert.ToDouble(timeBox.Text);
                            delay = (int)Math.Abs((delay * coef));

                        }
                        catch
                        {
                            delay = Math.Abs(aeroScopes[Ti1].time - aeroScopes[Ti].time);
                        }
                    }
                });

                await Task.Delay(delay);

                count4++;
            }
        }

        private async void TrajRequestRDM()
        {
            while (istrajRequestRDM)
            {
                point5 += 2.0;

                if (point5 >= 360) point5 = point5 - 360;

                if (count5 == aeroScopes.Count)
                {
                    count5 = 0;
                }

                float x = aeroScopes[count5].latitude;
                float z = aeroScopes[count5].longitude;
                float y = (float)aeroScopes[count5].altitude;

                //var ywidth = (float)(_chart.View3D.YAxisPrimary3D.Maximum - _chart.View3D.YAxisPrimary3D.Minimum);
                DispatchIfNecessary(() =>
                {
                    if (altCheckBox.IsChecked.Value)
                    {
                        var ywidth = (float)(_chart.View3D.YAxisPrimary3D.Maximum - _chart.View3D.YAxisPrimary3D.Minimum);
                        y = (float)Math.Cos(2 * Math.PI * point4 / 360) * (ywidth / 4) + (ywidth / 2);
                    }

                    DroneModelRDM.Position.X = (float)x;
                    DroneModelRDM.Position.Y = (float)y;
                    DroneModelRDM.Position.Z = (float)z;

                    _chart.View3D.Annotations[1].TargetAxisValues.SetValues(DroneModel.Position.X, DroneModel.Position.Y, DroneModel.Position.Z);
                    _chart.View3D.Annotations[1].Text = "X: " + DroneModel.Position.X.ToString("F6") + "\nY: " + DroneModel.Position.Y.ToString("F0") + "\nZ: " + DroneModel.Position.Z.ToString("F6");


                    if (lseriesPoint3DsRDM.Count < tail1)
                    {
                        lseriesPoint3DsRDM.Add(new SeriesPoint3D(x, y, z));
                    }
                    else
                    {
                        lseriesPoint3DsRDM.RemoveAt(0);
                        lseriesPoint3DsRDM.Add(new SeriesPoint3D(x, y, z));
                    }

                    if (lseries2Point3DsRDM.Count < tail2)
                    {
                        lseries2Point3DsRDM.Add(new SeriesPoint3D(x, y, z));
                        lseries2Point3DsRDM.Add(new SeriesPoint3D(x, 0, z));
                        lseries2Point3DsRDM.Add(new SeriesPoint3D(x, y, z));
                    }
                    else
                    {
                        lseries2Point3DsRDM.RemoveAt(0);
                        lseries2Point3DsRDM.RemoveAt(0);
                        lseries2Point3DsRDM.RemoveAt(0);
                        lseries2Point3DsRDM.Add(new SeriesPoint3D(x, y, z));
                        lseries2Point3DsRDM.Add(new SeriesPoint3D(x, 0, z));
                        lseries2Point3DsRDM.Add(new SeriesPoint3D(x, y, z));
                    }

                    dataSeriesRDM.Points = lseriesPoint3DsRDM.ToArray();

                    dataSeries2RDM.Points = lseries2Point3DsRDM.ToArray();

                });

                int Ti = count5;
                int Ti1 = count5 + 1;
                if (Ti1 == aeroScopes.Count) Ti1 = 0;

                int delay = Math.Abs(aeroScopes[Ti1].time - aeroScopes[Ti].time);
                if (delay > 10000) delay = 5000;
                //Console.WriteLine(delay);
                DispatchIfNecessary(() =>
                {
                    if (timeCheckBox.IsChecked.Value)
                    {
                        try
                        {
                            if (timeBox.Text.Contains("."))
                                timeBox.Text = timeBox.Text.Replace(".", ",");
                            double coef = Convert.ToDouble(timeBox.Text);
                            delay = (int)Math.Abs((delay * coef));

                        }
                        catch
                        {
                            delay = Math.Abs(aeroScopes[Ti1].time - aeroScopes[Ti].time);
                        }
                    }
                });

                await Task.Delay(delay);

                count5++;
            }
        }

        private void Button23_Click(object sender, RoutedEventArgs e)
        {
            istraj = !istraj;

            if (istraj)
            {
                istrajRequest = true;
                Task.Run(() => TrajRequest());
            }
            else
            {
                istrajRequest = false;
            }
        }

        private void Button231_Click(object sender, RoutedEventArgs e)
        {
            istrajRDM = !istrajRDM;

            if (istrajRDM)
            {
                istrajRequestRDM = true;
                Task.Run(() => TrajRequestRDM());
            }
            else
            {
                istrajRequestRDM = false;
            }
        }

        private void Button24_Click(object sender, RoutedEventArgs e)
        {
            lseriesPoint3Ds.Clear();
            lseries2Point3Ds.Clear();
        }

        private void ButtonTailCount_Click(object sender, RoutedEventArgs e)
        {
            int tailcount = 200;
            try
            {
                tailcount = Convert.ToInt32(tailCountBox.Text);
            }
            catch
            {
                tailcount = 200;
            }
            lseriesPoint3Ds.Clear();
            lseries2Point3Ds.Clear();
            tail1 = tailcount;
            tail2 = tail1 * 3;
        }


        bool rightpanel = true;
        private void Button26_Click(object sender, RoutedEventArgs e)
        {
            rightpanel = !rightpanel;

            if (rightpanel)
            {
                myGrid.ColumnDefinitions[1].Width = new GridLength(1, GridUnitType.Auto);
                myGrid.ColumnDefinitions[2].Width = new GridLength(1, GridUnitType.Auto);
            }
            else
            {
                myGrid.ColumnDefinitions[1].Width = new GridLength(0);
                myGrid.ColumnDefinitions[2].Width = new GridLength(0);
            }
        }

        private void ButtonDemo_Click(object sender, RoutedEventArgs e)
        {
            InitDemo();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            SaveToYaml();
        }

        private void SaveToYaml()
        {
            Settings3D settings3D = new Settings3D();
            settings3D.UDPmyIP = myIP.Text;
            settings3D.UDPmyPort = Int32.Parse(myPort.Text);
            settings3D.UDPremoteIP = remoteIP.Text;
            settings3D.UDPremotePort = Int32.Parse(remotePort.Text);
            YamlSave(settings3D);
        }

        private void VAero_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DroneModel.Visible = vAero.IsChecked.Value;
                //dataSeries.Visible = vAero.IsChecked.Value;
                //dataSeries2.Visible = vAero.IsChecked.Value;
            }
            catch { }
        }

        private void VRDM_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DroneModelRDM.Visible = vRDM.IsChecked.Value;
                //dataSeriesRDM.Visible = vRDM.IsChecked.Value;
                //dataSeries2RDM.Visible = vRDM.IsChecked.Value;
            }
            catch { }
        }

        private void ButtonDemoplus_Click(object sender, RoutedEventArgs e)
        {
            //демо
            InitDemo();
            //ввести имя файла
            trajBox.Text = "AeroScopeSamples84";
            //считать файл
            Button22_Click(this, null);
            //ввести кол-во точек хвоста
            tailCountBox.Text = "60";
            //применить кол-во точек хвоста
            ButtonTailCount_Click(this, null);
            //ускорить чутка мероприятие
            timeCheckBox.IsChecked = true;
            //запустить
            Button23_Click(this, null);
        }

        private void LegendBox_Click(object sender, RoutedEventArgs e)
        {
            _chart.View3D.LegendBox.Visible = !_chart.View3D.LegendBox.Visible;
        }

        private void Apply_Click(object sender, RoutedEventArgs e)
        {
            Button18_Click(this, null);


        }

        private void ButtonDemoplus2_Click(object sender, RoutedEventArgs e)
        {
            //Load obj
            {
                //Disable rendering, strongly recommended before updating chart properties
                _chart.BeginUpdate();

                MeshModel model = new MeshModel(
                _chart.View3D,
                Axis3DBinding.Primary,
                Axis3DBinding.Primary,
                Axis3DBinding.Primary);

                string temp = System.Reflection.Assembly.GetEntryAssembly().Location;
                var lastindex = temp.LastIndexOf("\\");
                temp = temp.Substring(0, lastindex);
                lastindex = temp.LastIndexOf("\\");
                temp = temp.Substring(0, lastindex);
                lastindex = temp.LastIndexOf("\\");
                temp = temp.Substring(0, lastindex);
                temp = temp + "\\Resources\\3d_metashape_1M.obj";
                model.LoadFromFile(temp);

                model.Position.X = (float)_chart.View3D.XAxisPrimary3D.Minimum + (float)(_chart.View3D.XAxisPrimary3D.Maximum - _chart.View3D.XAxisPrimary3D.Minimum) * 0.5f;
                model.Position.Y = 0;
                model.Position.Z = (float)_chart.View3D.ZAxisPrimary3D.Minimum + (float)(_chart.View3D.ZAxisPrimary3D.Maximum - _chart.View3D.ZAxisPrimary3D.Minimum) * 0.5f;

                model.Rotation.SetValues(0, 0, 0);

                model.Size.SetValues(1f, 1f, 1f);

                model.MouseInteraction = false;
                _chart.View3D.MeshModels.Add(model);

                //Allow chart rendering
                _chart.EndUpdate();
            }
            //size
            MeshSize_Click(this, null);
            //rotation
            Yrotation_Click(this, null);
            //visibility
            Button10_Click(this, null);
            //uav
            Button21_Click(this, null);

            //отключить видимость лишнего дрона
            vRDM.IsChecked = false;
            VRDM_Click(this, null);

            //кольца-области
            Button18_Click(this, null);

            trajBox.Text = "AeroScopeSamples78";
            //считать файл
            Button22_Click(this, null);
            //ввести кол-во точек хвоста
            tailCountBox.Text = "60";
            //применить кол-во точек хвоста
            ButtonTailCount_Click(this, null);
            //ускорить чутка мероприятие
            timeCheckBox.IsChecked = true;
            //запустить
            Button23_Click(this, null);


            //clear right panel
            yBox.Clear();
            meshSizeBox.Clear();
            //hide right panel
            Button26_Click(this, null);
        }

        private void _chart_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                if (_chart.View3D.Rectangles.Count > 0)
                {
                    for (int i = 0; i < _chart.View3D.Rectangles.Count; i++)
                    {
                        _chart.View3D.Rectangles[i].Visible = !_chart.View3D.Rectangles[i].Visible;
                    }
                }
            }
        }

        private void ReCoords_Click(object sender, RoutedEventArgs e)
        {
            double m_mapCoordCornerLat = Convert.ToDouble(latCorner.Text);
            double m_mapCoordCornerLon = Convert.ToDouble(lonCorner.Text);

            double m_mapCoordMaxLatLat = Convert.ToDouble(latMaxLat.Text);
            double m_mapCoordMaxLatLon = Convert.ToDouble(lonMaxLat.Text);

            double m_mapCoordMaxLonLat = Convert.ToDouble(latMaxLon.Text);
            double m_mapCoordMaxLonLon = Convert.ToDouble(lonMaxLon.Text);


            m_mapCoordCorner = new MapCoordinate(m_mapCoordCornerLat, m_mapCoordCornerLon);
            m_mapCoordMaxLat = new MapCoordinate(m_mapCoordMaxLatLat, m_mapCoordMaxLatLon);
            m_mapCoordMaxLon = new MapCoordinate(m_mapCoordMaxLonLat, m_mapCoordMaxLonLon);


            _chart.View3D.XAxisPrimary3D.SetRange(m_mapCoordCorner.Latitude, m_mapCoordMaxLat.Latitude);

            _chart.View3D.XAxisPrimary3D.Reversed = XBox.IsChecked.Value;

            _chart.View3D.ZAxisPrimary3D.SetRange(m_mapCoordCorner.Longitude, m_mapCoordMaxLon.Longitude);

            _chart.View3D.ZAxisPrimary3D.Reversed = ZBox.IsChecked.Value;
        }






        /*
        void TMtrPalette::MakePalette()
        {
            double dblue, dgreen, dred;
            int blue1, green1, red1, blue2, green2, red2;
            int i;
            COLORREF palette[256];                                   // 01/02/05
            unsigned char* lpPalette = 0;

            // Заполнение действующей палитры
            lpPalette = (unsigned char*)&palette[0];

            // Черно-белая палитра
            if (ColorStyle == MTRPGRAY)  // 14/12/04
            {
                int count = GetColorCount();

                for (i = 0; i < count; i++)
                {
                    *lpPalette++ = (unsigned char)(i * 128 / count + 64);
                *lpPalette++ = (unsigned char)(i * 128 / count + 64);
                *lpPalette++ = (unsigned char)(i * 128 / count + 64);
                *lpPalette++ = 0;
            }
        }
 else
   {
     // Заполнение "32" позиций цветной палитры для стиля "Color"
     // Первый диапазон (зеленый)
     int count = GetColorCount() / 4;
     if (count< 1) count = 1;

     blue1 =  48;  green1 =  64;  red1 =   0;    // 8 позиций
     blue2 =   0;  green2 = 208;  red2 =  96;

     dred   = (red2   - red1  ) / count ;
     dgreen = (green2 - green1) / count ;
     dblue  = (blue2  - blue1 ) / count ;

     for (i=0; i<count; i++ )
       {
        *lpPalette++ = (unsigned char)(red1   + dred* i);
        *lpPalette++ = (unsigned char)(green1 + dgreen* i);
        *lpPalette++ = (unsigned char)(blue1  + dblue* i);
        *lpPalette++ = 0;
       }


    // Второй диапазон (желтый)
    blue1 =   0;  green1 = 192;  red1 = 160;    // 8 позиций
     blue2 =   0;  green2 = 256;  red2 = 256;

     dred   = (red2   - red1  ) / count ;
     dgreen = (green2 - green1) / count ;
     dblue  = (blue2  - blue1 ) / count ;

     for (i = 0; i<count ; i++ )
       {
        * lpPalette++ = (unsigned char)(red1   + dred* i);
        * lpPalette++ = (unsigned char)(green1 + dgreen* i);
        * lpPalette++ = (unsigned char)(blue1  + dblue* i);
        * lpPalette++ = 0;
       }


// Третий диапазон (оранжевый)
count = GetColorCount() - count*2;
     if (count< 1) count = 1;

     blue1 =   0;  green1 = 224;  red1 = 240;     // 16 позиций
     blue2 =   0;  green2 =   0;  red2 = 144;

     dred   = (red2   - red1  ) / count ;
     dgreen = (green2 - green1) / count ;
     dblue  = (blue2  - blue1 ) / count ;

     for (i=0; i<count; i++ )
       {
        * lpPalette++ = (unsigned char)(red1   + dred* i);
        * lpPalette++ = (unsigned char)(green1 + dgreen* i);
        * lpPalette++ = (unsigned char)(blue1  + dblue* i);
        * lpPalette++ = 0;
       }
   }

  // Заполнение эталонной палитры
  // Инициализировать цвета матрицы
  // указатель на палитру и количество цветов (<256 )
  // при ошибке возвращает 0
  InitBaseColors(&palette[0], GetColorCount());
}
*/

        /*
        void MakeSecretFormula(int nA)
        {
            const int count = m_colors->Length / 4;
            fillColorsHelper(0, count, nA, 0, 64, 48, 96, 208, 0);
            fillColorsHelper(count, count, nA, 160, 192, 0, 255, 255, 0);
            fillColorsHelper(count * 2, m_colors->Length - count * 2, nA, 240, 224, 0, 144, 0, 0);
        }

        void fillColorsHelper(int nStartIdx, int nCount, int a, int startR, int startG, int startB,
        int endR, int endG, int endB)
        {
            int deltaRed = (endR - startR) / nCount;
            int deltaGreen = (endG - startG) / nCount;
            int deltaBlue = (endB - startB) / nCount;
            for (int i = 0; i < nCount; i++)
                m_colors[nStartIdx + i] = Color::FromArgb(a, startR + i * deltaRed, startG + i * deltaGreen, startB + i * deltaBlue);
        }
        */
    }
}
